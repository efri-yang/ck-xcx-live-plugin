// components/input-number/index.ts
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    themeType:{
      type:String,
      optionalTypes: [Number],
      value:'default'
    },
    max:{
      type: Number,
      optionalTypes: [String],
      value: 9999,
      observer:function(){
       
      }
    },
    min:{
      type: Number,
      optionalTypes: [String],
      value: 1
    },
    val:{
      type: Number,
      optionalTypes: [String],
      value: 1,
      observer:function(val){
        this.setData({
          numVal:val
        })
      }
    }
    
  },
  lifetimes:{
    attached: function () {
      this.setData({
        numVal:this.data.val
      })
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    numVal:1
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindinput(e){
      if(e.detail.value){
        let value= Number(e.detail.value);
        value = this.getValue(value);
        this.setData({
          numVal:value
        },()=>{
          this.triggerEvent("change",value);
        })
      }
    },
    bindblur(e){
      let value= Number(e.detail.value);
      value = this.getValue(value);
      this.setData({
        numVal:value
      },()=>{
        this.triggerEvent("change",value);
      })
    },
    reduce(){
      if(this.data.numVal <= this.data.min) return;
      let value= this.data.numVal-1;
      value = this.getValue(value);
      this.setData({
        numVal:value
      },()=>{
        this.triggerEvent("change",value);
      })
    },
    add(){
      if(this.data.numVal >= this.data.max && this.data.max !=-1) return;
      let value= this.data.numVal+1;
      value = this.getValue(value);
      this.setData({
        numVal:value
      },()=>{
        this.triggerEvent("change",value);
      })
    },
    getValue(value){
      if(this.data.max != -1){
        if(value > this.data.max){
          value = this.data.max;
        }
      }
      if(value < this.data.min){
        value = this.data.min;
      }
      return value;
    }
  }
})

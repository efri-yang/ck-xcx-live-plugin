import utils from './utils/util';
module.exports = {
  /**
   * @description:设置环境
   * @val {number} 1-测试  2-formal 3-正式 
   * @return {*}
   */
  setEnvConfig: utils.setEnvConfig,
  setStorage: utils.setStorage,
  getStorage: utils.getStorage
}
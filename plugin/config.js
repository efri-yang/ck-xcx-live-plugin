 let ckFrom = {
  course: 5, //课程，音频视频图文
  column: 9, //专栏
  exam: 11, //测评
  datum: 8, //资料
  activity: 7, //活动
  social: 10, //社群
  vip: 1, //vip
  svip: 23, //svip
  systemPresentColumn: 24,
  shopping: 12, //商城
  mall: 13,
  qa: 15, //提问
  answer: 16, //查看答案
  circusee: 17, // 问答围观
  presentCourse: 25,
  presentColumn: 26,
  presentVip: 27,
  presentSvip: 28,
  collageColumn: 29,
  collageCourse: 30,
  collageVip: 31,
  collageSvip: 32,
  socailReword: 33,
  liveReword: 35,
  collageActivity: 36,
  clockIn: 41, //打卡
  agentApply: 42,
  partnerApply: 43,
  branchApply: 44,
  cooApply: 138,
  agencyApply: 139,
  agent6Apply: 160,
  agent7Apply: 161,
  agent8Apply: 162,
  agent9Apply: 163,
  collagePackage: 48,
  package: 61, //知识套餐
  presentPackage: 62, //赠送知识套餐
  live: 51, // 直播
  reservation: 53, //约课
  word: 56, //词典
  flashSalesCourse: 66,
  flashSalesColumn: 67,
  flashSalesVip: 68,
  flashSalesSvip: 69,
  flashSalesCombo: 70,
  flashSalesExam: 71,
  flashSalesLive: 72,
  flashSalesPresentVip: 73,
  flashSalesPresentSvip: 74,
  flashSalesClockIn: 8075,
  flashSalesCamp: 8076,
  flashSalesSocial: 8077,
  flashSalesReservation: 8078,
  flashSalesVoiceTest: 8079,
  flashSalesMutuaTest: 8080,
  flashSalesActivity: 8081,
  flashSalesEdu: 8082,
  flashSalesEbooks: 8083,
  flashSalesPrivatclass: 8084,
  collageCamp: 9078,
  collageSocial: 9010,
  collageClockIn: 9041,
  collageItemBank: 9042,
  collageEdu: 9043,
  collageEbooks: 9044,
  collagePersonalLive: 9045, // 拼团 总的
  mutuaTest: 75, //交互测评
  form: 76, //表单
  voiceTest: 77, //语音测评
  camp: 78, //训练营
  booking: 86, //预约师资
  reReservation: 97, // 约课复训 买的是专场
  iosCharge: 100,
  iosVipPay: 101,
  iosSVipPay: 102,
  distributionColumnPay: 103,
  distributionCollageColumn: 104,
  video: 110,
  voice: 111,
  imgText: 112,
  privateClass: 115, // 小班课
  edu: 116, //教务系统线下课
  eduJiGou: 118, //教务系统机构课
  eduInviteCard: 117, //线下课邀请卡
  collageLive: 124,
  testpPaper: 125, //测评考试
  collagePrivateClass: 126,
  itemBank: 129, //练习
  ebooks: 142, //电子书
  circle: 181, //圈子
  circleTheme: 185, //主题圈子
  presentEbooks: 145, //赠送电子书
  presentCamp: 146, //赠送训练营
  notice: 170, //预告
  shopComment: 155, //商城评价
  task: 137, // 好友灌溉
  coupon: 92, // 优惠券
  collage: 121, // 拼团 总的
  personalLive: 180, // 拼团 总的
  seatFee: 187, //约课支持席位费
  shareholder: 183, //升级股东
  giftCard: 190, // 虚拟充值
  shareholderPayVoucher: 200, //股东付费抵用券
  valuableCoupon: 5000, //有价券
  agentApplys: {
     agentApply: 42,
     partnerApply: 43,
     branchApply: 44,
     cooApply: 138,
     agencyApply: 139,
     agent6Apply: 160,
     agent7Apply: 161,
     agent8Apply: 162,
     agent9Apply: 163,
   }
 };
 let relationTypeMap = { //营销类型
  flashSales: 1, //限时购
  collage: 2, //拼团
  coupon: 7, //优惠券
  shareholderCoupon: 20, //股东优惠券
  valuableCoupon: 23 //有价优惠券
 };
 let prodMapType = {
  1: 'VIP',
  2: 'VIP',
  3: 'VIP2',
  4: 'svip',
  5: '课程',
  6: '分类',
  7: '活动',
  8: '资料',
  9: '专栏',
  10: '社群',
  11: '测评',
  12: '商城',
  13: '独立商城',
  15: '问答',
  16: '付费问答',
  17: '问答围观',
  20: '余额利息',
  22: 'VIP2',
  23: 'SVIP',
  24: '赠送专栏',
  25: '赠送课程',
  26: '赠送专栏',
  27: '赠送VIP',
  28: '赠送SVIP',
  29: '拼团专栏',
  30: '拼团课程',
  31: '拼团VIP',
  32: '拼团SVIP',
  33: '社群打赏',
  35: '直播打赏',
  37: '代理商赠送（兑换码兑换）课程',
  38: '代理商赠送（兑换码兑换）专栏',
  41: '打卡',
  42: '服务商',
  43: '服务商',
  44: '服务商',
  138: '服务商',
  139: '服务商',
  160: '服务商',
  161: '服务商',
  162: '服务商',
  163: '服务商',
  51: '直播',
  53: '约课',
  56: '词典',
  61: '知识套餐',
  62: '买赠知识套餐',
  66: '限时购课程',
  67: '限时购专栏',
  68: '限时购VIP',
  69: '限时购SVIP',
  70: '限时购知识套餐',
  71: '限时购测评',
  72: '限时购直播',
  73: '买赠VIP(限时购)',
  74: '买赠SVIP(限时购)',
  75: '互动测评',
  76: '表单',
  77: 'AI.测评',
  78: '训练营',
  86: '预约',
  91: '代理商赠送（兑换码兑换）训练营',
  100: '充值余额',
  103: '内容市场',
  104: '内容拼团',
  110: '视频',
  111: '音频',
  112: '图文',
  115: '小班课',
  116: '面授课',
  125: '考试',
  129: '练习',
  138: '服务商',
  139: '服务商',
  142: '电子书',
  145: '赠送电子书',
  146: '赠送训练营',
  178: '投票赠礼',
  180: '直播间',
  181: '圈子',
  183: '升级股东',
  185: '主题圈子',
  187: '席位费',
  190: '虚拟币',
  200: '抵用券',
  5000: '有价券',
  8083: '限时购电子书',
  8084: '限时购小班课',
  9044: '拼团电子书'
 };
 let prodCourseType = {
   0: '视频',
   1: '音频',
   2: '图文',
 };


 let defaultProdImgDict = {
   1: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/vip.png',
   31: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/vip.png',
   23: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/svip.png',
   32: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/svip.png',
   7: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/activity_defult.png',
   8: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/datum_defult.png',
   11: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/examination_defult.png',
   76: 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/form_defult.png',
   'default': 'https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/21_material_admin/image/assets/images/defult270.png'
 };

 let  formRelateType = {
  '5-2':1,
  '5-1':2,
  '5-0':3,
  75:11,
  125:11,
  9:4,
  11:12,
  41:5,
  78:6,
  7:7,
  61:8,
  51:9,
  8:13,
  1:14,
  23:15,
  10:16,
  180:20,
  115: 18,
};

const nationList = [
  "汉族",
  "壮族",
  "满族",
  "回族",
  "苗族",
  "维吾尔族",
  "土家族",
  "彝族",
  "蒙古族",
  "藏族",
  "布依族",
  "侗族",
  "瑶族",
  "朝鲜族",
  "白族",
  "哈尼族",
  "哈萨克族",
  "黎族",
  "傣族",
  "畲族",
  "傈僳族",
  "仡佬族",
  "东乡族",
  "高山族",
  "拉祜族",
  "水族",
  "佤族",
  "纳西族",
  "羌族",
  "土族",
  "仫佬族",
  "锡伯族",
  "柯尔克孜族",
  "达斡尔族",
  "景颇族",
  "毛南族",
  "撒拉族",
  "布朗族",
  "塔吉克族",
  "阿昌族",
  "普米族",
  "鄂温克族",
  "怒族",
  "京族",
  "基诺族",
  "德昂族",
  "保安族",
  "俄罗斯族",
  "裕固族",
  "乌孜别克族",
  "门巴族",
  "鄂伦春族",
  "独龙族",
  "塔塔尔族",
  "赫哲族",
  "珞巴族"
]

 export default {
   ckFrom,
   relationTypeMap,
   prodMapType,
   prodCourseType,
   defaultProdImgDict,
   formRelateType,
   nationList
 }
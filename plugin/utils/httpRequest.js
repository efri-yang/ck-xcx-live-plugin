let login_runing = false;
let login_collect = [];
let login_request_count = 0;
//load提示加载
let Loading = {
  count: 0,
  start: function () {
    wx.showLoading({
      title: "加载中..."
    });
  },
  end: function () {
    wx.hideLoading();
  },
  show: function () {
    if (this.count === 0) {
      this.start();
    };
    this.count++;
  },
  hide: function () {
    if (this.count <= 0) return;
    this.count--;
    if (this.count <= 0) {
      this.end();
    }
  }
}

function HttpRequest(isShowLoading, url, sessionChoose, params, method, callBack, reject) {
  let token = wx.getStorageSync("roomToken");
  let envConfig = wx.getStorageSync('envConfig');
  //判断token是否存在
  // 存在就继续执行

  if (isShowLoading == true) {
    Loading.show();
  }
  let header = {
    'content-type': sessionChoose == 1 ? 'application/json' : 'application/x-www-form-urlencoded',
    'X-FROM':'livePullWxApp',
    'Authorization': 'Bearer ' + token
  }
  url = /^http/.test(url) ? url : (envConfig.apiUrl + url)
  params = Object.prototype.toString.call(params) == "[object Object]" ? params : {};
  params.fromApp = 'livemp';
  wx.request({
    url: url,
    data: params,
    dataType: "json",
    header: header,
    method: method,
    success: function (res) {
      //token 存在 还返回1002，这个时候就要强制
      if (res.data.statusCode == 200) {
        callBack(res.data.data);
      } else if(res.data.statusCode == 10001){
        //其他设备登录
        wx.showModal({
          title: '提示',
          content: res.data.msg,
          showCancel: false,
          success: (res) => {
            if (res.confirm) {
              wx.removeStorage("roomToken");
              wx.removeStorage("shopInfo")
              wx.redirectTo({
                url: '/pages/auth-user/index'
              })
            }
          }
        })
      } else if (res.data.msg == "未找到该用户" || res.data.statusCode == 402 || res.data.status == 403) {
        wx.removeStorage("roomToken");
        wx.removeStorage("shopInfo")
        wx.redirectTo({
          url: '/pages/auth-user/index'
        })
      }else {
        setTimeout(()=>{
          wx.showToast({
            title: res.data.msg,
            icon: "none",
            duration:3000,
            success:()=>{
              reject();
            }
          },500);
        });
      }
    },
    fail: function (res) {
      console.warn("HttpRequst当中wx.request fail 执行了：调用失败", res)
    },
    complete: function (res) {
      console.log(`请求 ${url} 发送的数据是：`, params, '返回的数据是:', res.data.data);
      if (isShowLoading) {
        Loading.hide();
      }
    }
  })
}

function get(url, datas = {}, isShowLoading = true,sessionChoose=1) {
  return new Promise(function (resolve, reject) {
    HttpRequest(isShowLoading, url, sessionChoose, datas, 'GET', resolve, reject)
  })
}

function post(url, datas = {}, isShowLoading = true,sessionChoose = 1) {
  return new Promise(function (resolve, reject) {
    HttpRequest(isShowLoading, url, sessionChoose, datas, 'POST', resolve, reject)
  })
}




export {
  HttpRequest,
  get,
  post
}
import utils from '../../utils/util';
const CHAT_PAGE_SIZE = 30; //聊天列表每次请求的条数
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShowSite: {
      type: Number,
      value: false
    },
    isShowArea: {
      type: Number,
      value: false
    },
    mgId: { //商品的id
      type: Number,
      value: 0
    },
    info:{ //地址信息
      type:Object,
      value:{}
    }
  },

  lifetimes: {
    attached: function () {

    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    siteList: [],
    areaList:[],
    areaLoadStatus:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    openSiteList() {
      utils.post("goods/getOfflineCity", {
        mgId: [this.data.mgId],
        goodType: 1
      }, true, 1).then(res => {
        this.setData({
          siteList: res
        })
      })
    },
    openAreaPopup(e) {
      let item = e.currentTarget.dataset.item;
      this._currSiteItem = item;
      this.setData({
        isShowSite:false,
        isShowArea: true
      })
    },
    openSitePopup(){
      this.setData({
        isShowArea:false,
        isShowSite: true
      })
    },
    openAreaList() {
      this._areaPage = 1;
      this.setData({
        areaList:[],
      },()=>{
        this._getAreaList();
      })
    },
    _getAreaList() {
      if(this.data.areaLoadStatus=='loading') return;
      this.setData({
        areaLoadStatus: "loading"
      })
      utils.post("goods/getOfflineSiteList", {
        cityId: this._currSiteItem.cityId,
        goodType: 1,
        mgId: [this.data.mgId],
        page: this._areaPage,
        pageSize: CHAT_PAGE_SIZE
      }, true, 1).then(res => {
        //设置加载状态
        if (res.data.length == 0 && this._areaPage == 1) {
          this.setData({
            areaLoadStatus: "none",
          })
        } else if ((!res.data.length && this._areaPage != 1) || (this._areaPage == 1 && res.data.length < CHAT_PAGE_SIZE)) {
          this.setData({
            areaLoadStatus: "end"
          })
        } else {
          this.setData({
            areaLoadStatus: ""
          })
        }
        this.setData({
          areaList: this.data.areaList.concat(res.data)
        })
      }).catch(()=>{
        this.setData({
          areaLoadStatus: ""
        })
      })
    },
    chooseArea(e){
      console.log("chooseArea",e.currentTarget.dataset)
      let data = e.currentTarget.dataset.item;
      this.setData({
        isShowArea:false,
        isShowSite:false
      })
      this.triggerEvent("chooseSelfPickArea",data)

    }
  }
})
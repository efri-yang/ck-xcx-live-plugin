import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    //是否开通二级商务号
    isOpenAppletSubMch: {
      type: Boolean,
      default: true
    },
    detailId: { //打赏的表示专门独立直播间用
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    //讲师+嘉宾
    rolesList: {
      type: Array,
      default: [],
      observer: function (newVal) {
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    info: {},
    chooseItem: {},
    chooseIndex: 0,
    chooseRoleItem: {},
    chooseRoleIndex: 0,
    num: 1,
    totalPrice:0,
    customMoneyVal: ""
  },

  /**
   * 组件的方法列表
   */
  methods: {


    //选择打赏的角色
    chooseRole(e) {
      let item = e.currentTarget.dataset.item;
      let index = e.currentTarget.dataset.index;
      console.log(item)
      this.setData({
        chooseRoleItem: item,
        chooseRoleIndex: index
      })
    },
    //选择打赏的礼物
    chooseGift(e) {
      let item = e.currentTarget.dataset.item;
      console.log(item)
      if (item.limit == 0) {
        wx.showToast({
          title: '当前赠送次数已达到上限！',
          icon: "none"
        })
        return;
      }
      this.setData({
        num: 1,
        totalPrice:item.price,
        chooseItem: item
      })
    },
    //选择打赏的金额
    chooseMoney(e) {
      let item = e.currentTarget.dataset.item;
      let index = e.currentTarget.dataset.index;
      this.setData({
        customMoneyVal: "",
        chooseItem: item,
        chooseIndex: index
      })
    },
    onReword() {
      //IOS&有价格 直接提示不支持打赏
      if (utils.isIos() && this.data.chooseItem.price * 1) {
        wx.showToast({
          title: '暂不支持iOS系统内打赏',
          icon: "none",
          success: () => {
            setTimeout(() => {
              this.setData({
                isShow: false
              })
            }, 1500);
          }
        });
      } else {
        let params = {
          fromType: 1,
          totalPrice: this.data.customMoneyVal || utils.toFixed(this.data.chooseItem.price * this.data.num,2),
          groupId: this.data.chooseRoleItem.userId,
          number: this.data.num,
        }
        if (this.data.detailId) {
          params.subSonProdId = this.data.detailId;
        }
        if (this.data.chooseItem.lgcId) {
          params.lgcId = this.data.chooseItem.lgcId;
        }
        this.triggerEvent("openOtherPlaceOrder", {
          otherParams: params
        })
      }
    },
    inputNumberChange(e) {
      console.log(e);
      this.setData({
        num: e.detail,
        totalPrice:utils.toFixed(this.data.chooseItem.price * e.detail,2)
      })
    },
    moneyInputBlur(e) {
      console.log(e)
      let val = e.detail.value;
      let info = this.data.info;
      if (/^\D{1}/.test(val)) {
        val = "";
      } else if (/^0{2,}/.test(val)) {
        val = val.substr(0, 1)
      } else if (/^0[1-9]+/.test(val)) {
        val = val.substring(1)
      } else if (/^((0?)|([1-9]\d*?))\.\D/.test(val)) {
        val = val.substring(0, val.indexOf(".") + 1)
      } else if (!/^((0?)|([1-9]\d*?))(.\d{0,2})?$/.test(val)) {
        let floatVal = parseFloat(val);
        val = isNaN(floatVal) ? '' : floatVal.toFixed(2);
      }
      if (e.type == 'blur') {
        val = parseFloat(val) <= 0 ? '' : (val > 1000 ? 1000 : val);
      }
      this.setData({
        customMoneyVal: val
      })
    },
    open() {
      this.setData({
        chooseItem: {},
        chooseIndex: 0,
        chooseRoleItem: {},
        chooseRoleIndex: 0,
        num: 1,
        customMoneyVal: ""
      })
      utils.get('liveFlow/getLiveConf', {
        socialRoomId: this.data.socialRoomId
      }).then(res => {
        if (res.rewardType == 2) {
          //打赏礼物，数据转化
          let arr = [];
          res.rewardGifts.forEach((item, index) => {
            let idx = Math.floor(index / 8);
            if (!arr[idx]) {
              arr[idx] = [item]
            } else {
              arr[idx].push(item)
            }
          });
          res.giftList = arr;
        }
        let chooseItem = res.rewardType == 1 ? res.rewardAmounts[0] : res.rewardGifts[0];
        this.setData({
          info: res,
          chooseRoleItem:this.data.rolesList[0],
          chooseItem: chooseItem,
          totalPrice:utils.toFixed(chooseItem.price,2)
        })
      })
    },
    show() {
      this.setData({
        isShow: true
      })
    },
    hide() {
      this.setData({
        isShow: false
      })
    }
  }
})
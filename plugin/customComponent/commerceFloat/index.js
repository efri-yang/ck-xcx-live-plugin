import config from '../../config';
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    zIndex: {
      type: Number,
      optionalTypes: [String],
      value: 0
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    liveId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    detailId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    info: {
      type: Object,
      value: {},
      observer:function(newVal){
        console.log("commerceFloat-newVal",newVal)
      }
    }
  },

  lifetimes: {
    attached: function () {
      this.setData({
        isIos: utils.isIos()
      });
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    }
  },





  /**
   * 组件的初始数据
   */
  data: {
    prodMapType: config.prodMapType,
    prodCourseType: config.prodCourseType,
    relationTypeMap: config.relationTypeMap,
    isIos: false,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //直接购买还是去引导
    switchBuyGuide() {
      this.triggerEvent("switchBuyGuide",this.data.info)
      
    },
   
    openCouponBuyGuide(e) {
      let data = e.currentTarget.dataset.item;
      console.log(data)
      wx.showToast({
        title: '小程序暂不支持购买',
        icon: "none"
      })
    },
    getCoupon() {
      let info = this.data.info;
      if (this._isRequesting) return;
      utils.getBindPhoneSetting(config.ckFrom.coupon).then((res) => {
        if (res.getBind) {
          wx.showToast({
            title: '请绑定手机号码',
          });
          this.triggerEvent("bindPhone")
          this._isRequesting = false;
        } else {
          let url = '',
            data = {
              source: 13,
              SocialRoomId: this.data.socialRoomId
            };
          if (this.data.detailId) {
            data.detailId = this.data.detailId;
          }
          if (info.prodType == 200) {
            url = `coupon/collectJWCoupon`;
            data.redeemCodeId = info.relationId;
          } else {
            url = `redeemCodes/collectRedeemCode/${info.relationId}`;
          }
          utils.post(url, data).then(res => {
            this._isRequesting = false;
            if (res.status == 1) {
              wx.showToast({
                title: '领取成功，请在“个人中心>优惠券”中查看',
                icon: "none"
              })
            } else {
              wx.showToast({
                title: res.detail ? res.detail : res.msg,
                icon: "none"
              })
            }
            this.onClose();
          }).catch(() => {
            this._isRequesting = false;
          })
        }
      }).catch(() => {
        this._isRequesting = false;
      })
    },
    onClose() {
      this.setData({
        isShow: false
      })
    }
  }
})
// customComponent/invitationCard/index.ts
import utils from '../../utils/util';
import IM from '../../utils/im';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    liveId: {
      type: Number,
      value: 0
    },
    isSendMsg:{
      type: Boolean,
      value: true
    },
    ckFrom:{
      type:Number,
      value:51
    },
    detailId:{
      type: Number,
      value: 0
    },
    userId: {
      type: String,
      value: ""
    }
  },

  lifetimes: {
    created() {

    },
    attached() {

    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    shareList: [],
    shareIdx: 0,
    canvasDict: {
      w: 750,
      h: 1334,
      isDrawing: false,
      qrcodeTemp: "", //二维码tempFile,避免重复download,
      bannerTemp: "", //banner
      avatarTemp: "",
      bgImgTemp: [],
      canvasImg: [], //已生成的图片，第二次点击直接使用
    },
    srcTest:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    async onOpen() {
      if (!this.data.canvasDict.canvasImg[0]) {
        await utils.get('company/share', {
          reservedRefereeId: this.data.userId
        }, false).then(res => {
          this.companyInfo = res;
        })
        utils.get('common/inviteShow', {
          cId: 0,
          extId: this.data.liveId,
          ckFrom: this.data.ckFrom
        }).then(res => {
          this._initShareStatics(res);
          const query = this.createSelectorQuery();
          query.select('#canvas').fields({
              node: true,
              size: true
            })
            .exec(this.init.bind(this));
        })
      }else{
        if(this.data.isSendMsg){
          IM.sendCustomMessage({
            ...IM.options.ctx._imSendDefaultData,
            socialRoomMessageId: new Date().getTime(),
            talkType:1,
            msgType: 82
          })
        }
      }
    },

    init(res) {
      const width = res[0].width;
      const height = res[0].height;
      const canvas = res[0].node;
      const ctx = canvas.getContext('2d');
      canvas.width = width
      canvas.height = height;
      this.canvas = canvas;
      this.ctx = ctx;
      wx.showLoading();
      this._render().then(() => {
        this._canvasToTempFilePath().then(() => {
          wx.hideLoading();
        }).catch(() => {
          wx.hideLoading();
        });
      }).catch(() => {
        wx.hideLoading();
      });
    },
    _render() {
      return new Promise((resolve, reject) => {
        let item = this.data.shareList[this.data.shareIdx];
        let bgTemp = this.data.canvasDict.bgImgTemp[this.data.shareIdx];
        this._setDrawingStatus(true);
        console.log("item.bgImg.src",item)
        if (item.bgImg.src) {
          if(this.data.isSendMsg){
            IM.sendCustomMessage({
              ...IM.options.ctx._imSendDefaultData,
              socialRoomMessageId: new Date().getTime(),
              talkType:1,
              msgType: 82
            })
          }
          if (!bgTemp) {
            console.log("下载bgImg")
            //自定义的可能没有背景图片
            this._drawImage(item.bgImg).then(res => {
              this._drawBody(item, resolve);
            })
          } else {
            console.log("已经有了tempbg");
            this.ctx.drawImage(bgTemp, item.x, item.y, item.w, item.h);
            this._drawBody(item, resolve);
          }
        } else {
          // that._drawBody(item, resolve);
        }
      })
    },
    preivewImg() {
      wx.previewImage({
        current: this.data.canvasDict.canvasImg[0],
        urls: [this.data.canvasDict.canvasImg[0]],
      })
    },
    //显示生成海报预览
    _canvasToTempFilePath: function () {
      return new Promise((resolve, reject) => {
        wx.canvasToTempFilePath({
          x: 0,
          y: 0,
          width: this.data.canvasDict.w,
          height: this.data.canvasDict.h,
          destWidth: this.data.canvasDict.w,
          destHeight: this.data.canvasDict.h,
          canvas: this.canvas,
          success: (res) => {
            let arr = this.data.canvasDict.canvasImg;
            arr[this.data.shareIdx] = res.tempFilePath;
            this.setData({
              ['canvasDict.canvasImg']: arr
            });
            resolve();
          },
          fail: function () {
            console.log("_canvasToTempFilePath fail")
          }
        })
      })
    },


    _drawBody: function (item, resolve) {
      let arr= [
        this._drawRoundRect(item.roundRect),
        this._drawImage(item.avatar, "avatarTemp"),
        this._drawTxt(item.nickMark),
        this._drawTxt(item.nickMark1),
        this._drawTxt(item.title),
        this._drawTxt(item.callTag),
        this._drawTxt(item.froms),
        this._drawTxt(item.copyright),
        this._drawImage(item.banner, "bannerTemp"),
        this._drawImage(item.qrcode, "qrcodeTemp"),
      ]
      if(!this.data.detailId){
        arr.push(this._drawTxt(item.liveStartTime));
      }
      Promise.all(arr).then(res => {
        setTimeout(() => {
          this._setDrawingStatus(false);
          resolve();
        }, 1000)
      }).catch(()=>{
        wx.hideLoading();
        wx.showToast({
          title: '当前设备暂不支持分享功能，请前往公众号分享',
          icon:"none",
          duration: 2500
        });
        this.setData({
          isShow:false
        })
      })
    },
    //设置绘画
    _setDrawingStatus: function (val) {
      this.setData({
        ['canvasDict.isDrawing']: val
      })
    },
    //绘制二维码
    _drawImage: function (item) {
      return new Promise((resolve,reject) => {
        let ctx = this.ctx;
        const img = this.canvas.createImage();
        img.src = item.src;
        img.onload = () => {
          if (item.isCircle) {
            ctx.save();
            ctx.beginPath();
            ctx.arc(item.x + item.w / 2, item.y + item.h / 2, item.w / 2, 0, 2 * Math.PI, false);
            ctx.clip();
            ctx.drawImage(img, item.x, item.y, item.w, item.h);
            ctx.closePath();
            ctx.restore();
          } else {
            console.log("_drawImage", item)
            ctx.drawImage(img, item.x, item.y, item.w, item.h);
          }
          resolve();
        }
        img.onerror= (res)=>{
          console.log("onerror",res);
          reject();
        }
      })
    },
    //填充圆角矩形
    _drawRoundRect(item) {
      return new Promise((resolve) => {
        if (!item) resolve();
        let ctx = this.ctx;
        let radius = item.radius;
        ctx.save();
        ctx.translate(item.x, item.y);
        //绘制圆角矩形的各个边  
        if (!Array.isArray(radius)) {
          radius = [radius, radius, radius, radius]
        }
        ctx.beginPath();
        ctx.arc(item.w - radius[2], item.h - radius[2], radius[2], 0, Math.PI / 2);
        ctx.lineTo(radius[0], item.h);
        ctx.arc(radius[3], item.h - radius[3], radius[3], Math.PI / 2, Math.PI);
        ctx.lineTo(0, radius[3]);
        ctx.arc(radius[0], radius[0], radius[0], Math.PI, Math.PI * 3 / 2);
        ctx.lineTo(item.w - radius[0], 0);
        ctx.arc(item.w - radius[1], radius[1], radius[1], Math.PI * 3 / 2, Math.PI * 2);
        ctx.lineTo(item.w, item.h - radius[1]);
        ctx.closePath();
        //填充颜色
        ctx.fillStyle = item.fillColor;
        ctx.fill();

        ctx.restore();
        resolve();
      })
    },
    //绘制文本
    _drawTxt: function (textObj) {
      return new Promise((resolve) => {
        if (!textObj) resolve();
        this.ctx.textBaseline = "top";
        this.ctx.fillStyle = textObj.color;
        this.ctx.font = textObj.fontSize + "px Arial";
        this.ctx.textAlign = textObj.align;
        let textW = this._calcTextWith(textObj.val);
        let x = 0;
        if (textObj.align == "center") {
          x = textObj.full ? this.data.canvasDict.w / 2 : (textObj.maxW ? (textObj.x + textObj.maxW / 2) : this.data.canvasDict.w / 2);
        } else {
          x = textObj.x;
        }
        if (textW > textObj.maxW) {
          //文字的宽度大于规定最大的宽度，进行换行绘画，同时行数要小于设定的值；多行的时候根据参数是往上移动还是往下移动
          let arrLine = this._getTextLine(textObj);
          let len = textObj.limitline ? (arrLine.length > textObj.limitline ? textObj.limitline : arrLine.length) : arrLine.length;
          for (let i = 0; i < len; i++) {
            let y = textObj.dir == "up" ? textObj.y - (len - i - 1) * (textObj.fontSize + (textObj.distance || 5)) : textObj.y + i * (textObj.fontSize + (textObj.distance || 5));
            if (i < len) {
              if (i == len - 1 && arrLine.length > textObj.limitline) {
                arrLine[i] = arrLine[i].substring(0, arrLine[i].length - 6) + '......' + textObj.val.substring(textObj.val.length - 2)
              }
              this.ctx.fillText(arrLine[i], x, y, textObj.maxW);
            }
          }
        } else {
          //直接画文字
          this.ctx.fillText(textObj.val, x, textObj.y, textObj.maxW);
        }

        resolve();
      })

    },
    //获取文本行数
    _getTextLine: function (obj) {
      let arrText = obj.val.split('');
      let line = '';
      let arrline = [];
      for (let i = 0; i < arrText.length; i++) {
        let currLine = line + obj.val[i];
        let metrics = this.ctx.measureText(currLine);
        if (metrics.width > obj.maxW) {
          arrline.push(line);
          line = obj.val[i];
        } else {
          line = currLine;
          if (i == obj.val.length - 1) {
            arrline.push(line);
          }
        }
      }
      return arrline;
    },
    //计算文本宽度
    _calcTextWith: function (txt) {
      const metrics = this.ctx.measureText(txt)
      return metrics.width;
    },
    //
    _getCustomQrcode: function (dep) {
      let res = {
        w: 160,
        h: 160,
        y: 950
      };
      switch (dep.qrcodeStyle) {
        case 1:
          console.log(1);
          res.x = 70;
          break;
        case 2:
          console.log(2);
          res.x = 295;
          break;
        case 3:
          console.log(2);
          res.x = 295;
          break;
        default:
          res.x = 540;
      }
      return res;
    },
    _getCustomAvatar: function (dep) {
      let res = {};
      switch (dep.userType) {
        case 1:
          res = {
            x: 70,
            y: 60
          }
          break;
        case 2:
          res = {
            x: 315,
            y: 60,
          }
          break;
        case 3:
          res = {
            x: 315,
            y: 1000,
          }
          break;
        default:
          res = {
            x: 70,
            y: 60
          }
      }
      return res;
    },

    _getCustomNickMark: function (dep, type) {
      let res = {};
      switch (dep.userType) {
        case 1:
          res = {
            x: 70,
            align: "left",
            y: type == 1 ? 200 : 250
          }
          break;
        case 2:
          res = {
            align: "center",
            full: true,
            y: type == 1 ? 200 : 250,
          }
          break;
        case 3:
          res = {
            x: 60,
            align: "left",
            y: type == 1 ? 1140 : 1190,
          }
          break;
        default:
          res = {
            x: 70,
            align: "left",
            y: type == 1 ? 200 : 250
          }
      }
      res.color = dep.wordColor;
      return res;
    },

    _initShareStatics: function (obj) {
      /**
       * qrcodeStyle: 1下左 2下中 3 下右 
       * userStyle: 1
       * userType: 1上左 2上中 3 下右  -1 不显示
       * wordColor: "#333" 
       */
      let initData = [{ //第1张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 315,
          y: 170,
          w: 120,
          h: 120,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某邀请你一起加入
          x: 125,
          y: 304,
          maxW: 500,
          color: "#666",
          fontSize: 24,
          align: "center",
          full: true,
          val: ""
        },
        nickMark1: { //邀请你一起加入
          x: 110,
          y: 323,
          maxW: 530,
          color: "#666666",
          fontSize: 24,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 125,
          y: 365,
          w: 480,
          h: 320,
          src: ""
        },
        title: { //标题
          x: 125,
          y: 712,
          maxW: 500,
          color: "#A33838",
          fontSize: 32,
          align: "center",
          full: true,
          limitline: 2,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 125,
          y: 814,
          maxW: 500,
          limitline: 1,
          align: 'center',
          color: "#333333",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 125,
          y: 860,
          maxW: 500,
          align: 'center',
          color: "#333333",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 275,
          y: 935,
          w: 200,
          h: 200
        },
        froms: {
          x: 125,
          y: 1156,
          maxW: 500,
          align: 'center',
          fontSize: 24,
          color: "#A33838",
          limitline: 2,
          val: ""
        },
        copyright: {
          x: 50,
          y: 1277,
          maxW: 650,
          align: 'center',
          fontSize: 20,
          color: "#fff",
          limitline: 1,
          full: true,
          val: ""
        }
      }, { //第2张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 315,
          y: 170,
          w: 120,
          h: 120,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某邀请你一起加入
          x: 125,
          y: 304,
          maxW: 500,
          color: "#A33838",
          fontSize: 24,
          align: "center",
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 125,
          y: 365,
          w: 480,
          h: 320,
          src: ""
        },
        title: { //标题
          x: 125,
          y: 712,
          maxW: 500,
          color: "#A33838",
          fontSize: 32,
          align: "center",
          full: true,
          limitline: 2,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 125,
          y: 814,
          maxW: 500,
          limitline: 1,
          align: 'center',
          color: "#333333",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 125,
          y: 860,
          maxW: 500,
          align: 'center',
          color: "#333333",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 275,
          y: 935,
          w: 200,
          h: 200
        },
        froms: {
          x: 125,
          y: 1156,
          maxW: 500,
          align: 'center',
          fontSize: 24,
          color: "#A33838",
          limitline: 2,
          val: ""
        },
        copyright: {
          x: 50,
          y: 1277,
          maxW: 650,
          align: 'center',
          fontSize: 20,
          color: "#fff",
          limitline: 1,
          full: true,
          val: ""
        }
      }, { //第3张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 315,
          y: 67,
          w: 120,
          h: 120,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某邀请你一起加入
          x: 150,
          y: 200,
          maxW: 400,
          color: "#DCC5B0",
          fontSize: 24,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 135,
          y: 260,
          w: 480,
          h: 320,
          src: ""
        },
        title: { //标题
          x: 135,
          y: 600,
          maxW: 480,
          color: "#DCC5B0",
          fontSize: 32,
          align: "center",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 135,
          y: 700,
          maxW: 500,
          limitline: 1,
          align: 'center',
          color: "#DCC5B0",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 135,
          y: 750,
          maxW: 480,
          align: 'center',
          color: "#DCC5B0",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: obj.qrcode,
          x: 275,
          y: 845,
          w: 200,
          h: 200
        },
        froms: {
          x: 135,
          y: 1065,
          maxW: 480,
          align: 'center',
          fontSize: 24,
          color: "#999",
          limitline: 2,
          val: ""
        },
        copyright: {
          x: 50,
          y: 1277,
          maxW: 650,
          align: 'center',
          fontSize: 20,
          color: "#fff",
          limitline: 1,
          val: ""
        }
      }, { //第4张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 315,
          y: 170,
          w: 120,
          h: 120,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某邀请你一起加入
          x: 110,
          y: 304,
          maxW: 530,
          color: "#666666",
          fontSize: 24,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 135,
          y: 365,
          w: 480,
          h: 320,
          src: ""
        },
        title: { //标题
          x: 110,
          y: 712,
          maxW: 530,
          color: "#333333",
          fontSize: 32,
          align: "center",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 135,
          y: 820,
          maxW: 500,
          limitline: 1,
          align: 'center',
          color: "#666666",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 110,
          y: 864,
          maxW: 530,
          align: 'center',
          color: "#666666",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 275,
          y: 936,
          w: 200,
          h: 200
        },
        froms: {
          x: 110,
          y: 1156,
          maxW: 530,
          align: 'center',
          fontSize: 24,
          color: "#999",
          limitline: 2,
          full: true,
          val: ""
        },
        copyright: {
          x: 50,
          y: 1277,
          maxW: 650,
          align: 'center',
          fontSize: 20,
          color: "#fff",
          limitline: 1,
          val: ""
        }
      }, { //第5张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 22,
          y: 51,
          w: 100,
          h: 100,
          isCircle: true,
          src: ""
        },
        roundRect: {
          x: 0,
          y: 40,
          w: 400,
          h: 120,
          radius: [0, 50, 50, 0],
          fillColor: "rgba(255,255,255,0.5)"
        },
        nickMark: { //某某
          x: 140,
          y: 87,
          maxW: 530,
          color: "#666666",
          fontSize: 28,
          align: "left",
          limitline: 1,
          full: false,
          val: ""
        },
        nickMark1: { //邀请你一起加入
          x: 110,
          y: 323,
          maxW: 530,
          color: "#666666",
          fontSize: 24,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 150,
          y: 370,
          w: 450,
          h: 300,
          src: ""
        },
        title: { //标题
          x: 120,
          y: 700,
          maxW: 510,
          color: "#333333",
          fontSize: 36,
          align: "center",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 120,
          y: 817,
          maxW: 510,
          limitline: 1,
          align: 'center',
          color: "#666666",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 110,
          y: 860,
          maxW: 530,
          align: 'center',
          color: "#666666",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 275,
          y: 992,
          w: 200,
          h: 200
        },
        froms: {
          x: 120,
          y: 947,
          maxW: 510,
          align: 'center',
          fontSize: 28,
          color: "#666",
          limitline: 2,
          distance: 13,
          dir: "up",
          full: true,
          val: ""
        },
        copyright: {
          x: 50,
          y: 1277,
          maxW: 650,
          align: 'center',
          fontSize: 20,
          color: "#666",
          limitline: 1,
          val: ""
        }
      }, { //第6张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 315,
          y: 170,
          w: 120,
          h: 120,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某
          x: 135,
          y: 304,
          maxW: 480,
          color: "#D7BBA1",
          fontSize: 24,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 135,
          y: 365,
          w: 480,
          h: 300,
          src: ""
        },
        title: { //标题
          x: 110,
          y: 690,
          maxW: 550,
          color: "#D7BBA1",
          fontSize: 32,
          align: "center",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 110,
          y: 804,
          maxW: 550,
          limitline: 1,
          align: 'center',
          color: "#D7BBA1",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 110,
          y: 855,
          maxW: 550,
          align: 'center',
          color: "#D7BBA1",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 275,
          y: 935,
          w: 200,
          h: 200
        },
        froms: {
          x: 110,
          y: 1156,
          maxW: 530,
          align: 'center',
          fontSize: 26,
          color: "#D7BBA1",
          limitline: 2,
          full: true,
          val: ""
        },
        copyright: {
          x: 110,
          y: 1280,
          maxW: 530,
          align: 'center',
          fontSize: 20,
          color: "#fff",
          limitline: 1,
          val: ""
        }
      }, { //第7张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 85,
          y: 1060,
          w: 90,
          h: 90,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某
          x: 195,
          y: 1070,
          maxW: 400,
          color: "#333",
          fontSize: 28,
          align: "left",
          limitline: 1,
          full: true,
          val: ""
        },
        nickMark1: { //某某
          x: 195,
          y: 1115,
          maxW: 400,
          color: "#999",
          fontSize: 24,
          align: "left",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 85,
          y: 305,
          w: 580,
          h: 385,
          src: ""
        },
        title: { //标题
          x: 100,
          y: 710,
          maxW: 550,
          color: "#333",
          fontSize: 32,
          align: "left",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 100,
          y: 814,
          maxW: 550,
          limitline: 1,
          align: 'left',
          color: "#999",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 100,
          y: 860,
          maxW: 550,
          align: 'left',
          color: "#999",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 545,
          y: 1050,
          w: 110,
          h: 110
        },
        froms: {
          x: 110,
          y: 1190,
          maxW: 530,
          align: 'center',
          fontSize: 24,
          color: "#999",
          limitline: 2,
          full: true,
          val: ""
        },
        copyright: {
          x: 110,
          y: 1290,
          maxW: 530,
          align: 'center',
          fontSize: 24,
          color: "#fff",
          limitline: 1,
          val: ""
        }
      }, { //第8张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        avatar: { //背景头像
          x: 315,
          y: 125,
          w: 120,
          h: 120,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某
          x: 125,
          y: 270,
          maxW: 500,
          color: "#333",
          fontSize: 28,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        nickMark1: { //邀请你加入
          x: 125,
          y: 325,
          maxW: 500,
          color: "#999",
          fontSize: 24,
          align: "center",
          limitline: 1,
          full: true,
          val: ""
        },
        banner: { //banner图片
          x: 125,
          y: 390,
          w: 500,
          h: 333,
          src: ""
        },
        title: { //标题
          x: 100,
          y: 743,
          maxW: 550,
          color: "#333",
          fontSize: 32,
          align: "left",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 100,
          y: 844,
          maxW: 550,
          limitline: 1,
          align: 'left',
          color: "#999",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 100,
          y: 885,
          maxW: 550,
          align: 'left',
          color: "#999",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 280,
          y: 980,
          w: 190,
          h: 190
        },
        froms: {
          x: 110,
          y: 1180,
          maxW: 530,
          align: 'center',
          fontSize: 24,
          color: "#999",
          limitline: 2,
          full: true,
          val: ""
        },
        copyright: {
          x: 110,
          y: 1290,
          maxW: 530,
          align: 'center',
          fontSize: 24,
          color: "#666",
          limitline: 1,
          val: ""
        }
      }, { //第9张
        bgImg: {
          x: 0,
          y: 0,
          src: "",
          w: this.data.canvasDict.w,
          h: this.data.canvasDict.h
        }, //背景图片
        roundRect: {
          x: 0,
          y: 40,
          w: 400,
          h: 120,
          radius: [0, 10, 10, 0],
          fillColor: "rgba(255,255,255,0.5)"
        },
        avatar: { //背景头像
          x: 22,
          y: 51,
          w: 100,
          h: 100,
          isCircle: true,
          src: ""
        },
        nickMark: { //某某
          x: 140,
          y: 87,
          maxW: 420,
          color: "#666666",
          fontSize: 28,
          align: "left",
          limitline: 1,
          full: false,
          val: ""
        },
        nickMark1: { //邀请你一起加入
          x: 110,
          y: 323,
          maxW: 530,
          color: "#666666",
          fontSize: 28,
          align: "center",
          limitline: 1,
          full: false,
          val: ""
        },
        banner: { //banner图片
          x: 150,
          y: 390,
          w: 450,
          h: 300,
          src: ""
        },
        title: { //标题
          x: 120,
          y: 720,
          maxW: 510,
          color: "#333333",
          fontSize: 36,
          align: "center",
          limitline: 2,
          full: true,
          val: ""
        },
        callTag: { //讲师：某某剖
          x: 120,
          y: 827,
          maxW: 510,
          limitline: 1,
          align: 'center',
          color: "#666",
          fontSize: 24,
          val: ""
        },
        liveStartTime: { //直播开始时间
          x: 110,
          y: 884,
          maxW: 530,
          align: 'center',
          color: "#666666",
          limitline: 1,
          fontSize: 24,
          val: ""
        },
        qrcode: { //二维码
          src: "",
          x: 465,
          y: 1060,
          w: 160,
          h: 160
        },
        froms: {
          x: 110,
          y: 1080,
          maxW: 330,
          align: 'left',
          fontSize: 26,
          color: "#666",
          limitline: 3,
          dir: "down",
          full: true,
          val: ""
        },
        copyright: {
          x: 120,
          y: 1305,
          maxW: 500,
          align: 'center',
          fontSize: 28,
          color: "#666",
          full: true,
          limitline: 1,
          val: ""
        }
      }]
      let showName = obj.nickname || obj.realName;
      let lists = [];
     
      if (obj.custom.isCustom) {
        //自定义（后端已经返回了自定义的项，前端根据isShow来判断是否显示） 其他项没有isShow 属性
        for(let i=0;i<obj.custom.data.content.length;i++){
          let item = initData[i];
          let pitem = obj.custom.data.content[i];
          if (pitem.hasOwnProperty('isShow') && pitem.isShow) {
            //自定义海报选项参数设置  qrcode 的位置   头像位置    用户名文案颜色
            let qrcode = this._getCustomQrcode(pitem);
            let avatar = this._getCustomAvatar(pitem);
            let nickMark = this._getCustomNickMark(pitem, 1);
            let nickMark1 = this._getCustomNickMark(pitem, 2);
            item.qrcode = Object.assign(item.qrcode, qrcode);
            item.isCustom = 1; //页面上精准判断每个选项显示隐藏，自定义根据isShow 其他的时候根据 bgImg
            item.avatar = Object.assign(item.avatar, avatar);
            //1 某某  2：邀请你一起看直播
            item.nickMark = Object.assign(item.nickMark, nickMark);
            item.nickMark1 = Object.assign(item.nickMark1, nickMark1);
          }
          item.bgImg.src = pitem.bgImage;
          item.avatar.src = obj.avatar;
          item.banner.src = obj.banner;
          item.qrcode.src = obj.qrcodeUrl;
          item.nickMark.val = item.nickMark1 ? showName : (showName + " " + (pitem.customWords || pitem.word));
          item.nickMark1 && (item.nickMark1.val = (pitem.customWords || pitem.word));
          item.callTag.val = obj.tag + "：" + showName;
          item.liveStartTime.val = obj.startAt;
          item.title.val = obj.title;
          item.froms.val = "来自 [" + this.companyInfo.companyName + ']';
          item.copyright.val = this.companyInfo.companyAuth.instruction || '创客匠人技术支持';
          lists.push(item);
          break;
        }
      } else {
        //非自定义
        for (let i in obj.custom.data.bgs) {
          let item = initData[i];
          item.bgImg.src = obj.custom.data.bgs[i];
          item.avatar.src = obj.avatar;
          item.banner.src = obj.banner;
          // 
          item.qrcode.src = 'data:image/svg+xml;base64,'+ obj.qrcodeUrl;
          item.nickMark.val = item.nickMark1 ? showName : (showName + " " + obj.custom.data.words[0].content);
          item.nickMark1 && (item.nickMark1.val = obj.custom.data.words[0].content);
          item.callTag.val = obj.tag + "：" + showName;
          item.liveStartTime.val = obj.startAt;
          item.title.val = obj.title;
          item.froms.val = "来自 [" + this.companyInfo.companyName + ']';
          item.copyright.val = this.companyInfo.companyAuth.instruction || '创客匠人技术支持';
          lists.push(item);
          break;
        }
      }
      // console.log(lists, obj.qrcodeUrl);

      this.setData({
        shareList: [lists[0]]
      });

      // this._preDownload(lists);
    },
    _preDownload: function (data) {
      data.forEach((item, index) => {
        if (item.bgImg.src) {
          wx.downloadFile({
            url: item.bgImg.src,
            success: (res) => {
              let arr = this.data.canvasDict.bgImgTemp;
              arr[index] = res.tempFilePath;
              this.setData({
                ['canvasDict.bgImgTemp']: arr
              });
            }
          })
        }
      })
    }
  }
})
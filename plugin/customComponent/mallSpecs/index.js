import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    params: {
      type: Object,
      value: {}
    }
  },

  lifetimes: {
    attached: function () {
      // this._payType = 0; //0是微信支付，1是余额支付，2是虚拟币支付
      this._companyShare = wx.getStorageSync('companyShare');
      console.log("_companyShare", this._companyShare.companyAuth.companyName);
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    // info: "",
    specList: [],
    chooseSpecGroup: [],
    chooseProInfo: {},
    proBuyNum: 1

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onOpen() {
      this.setData({
        proBuyNum: 1
      });
      let res = this.data.params;
      this._cashBack = res.cashBack;
      this._specItemInfos = res.specItemInfos;
      this._goods = res.goods;
      this._banners = res.banners;
      this._agentConfs = res.agentConfs;

      let specList = this._getSpecList(res.specs, res.specItems);

      this.setData({
        info: res,
        specList: specList,
        chooseSpecGroup: this._goods.defaltSpec,
        proBuyNum: this._goods.minOrderQuantity
      });
      if (res.specs.length) {
        this._setChooseProInfo(this._goods.defaltSpec);
      } else {
        this.setData({
          chooseProInfo:this._goods
        })
      }
    },
    //确定规格
    specsConfirm() {
      let data = this.data.chooseProInfo;
      let sendData = {
        isLive: 1,
        mgId: this.data.info.goods.mgId,
        socialRoomId: this.data.socialRoomId,
        quantity: this.data.proBuyNum,
        gsiiId: this.data.chooseProInfo.gsiiId || 0
      }
      console.log("specsConfirm", data, sendData);
      this.triggerEvent("confirm", sendData);
    },
    //选中规格
    chooseSpec(e) {
      console.log("chooseSpec", e);
      let data = e.currentTarget.dataset.item;
      let pIdx = e.currentTarget.dataset.pidx;
      let chooseSpecGroup = this.data.chooseSpecGroup;
      chooseSpecGroup[pIdx] = data.gsiId;
      this.setData({
        proBuyNum:1,
        chooseSpecGroup: chooseSpecGroup
      });
      this._setChooseProInfo(chooseSpecGroup);
    },
    inputNumberChange(e) {
      console.log("e", e);
      this.setData({
        proBuyNum: e.detail
      })
    },
    _setChooseProInfo(chooseGroup) {
      let res = {};
      let lists = this._specItemInfos || [];
      for (let i = 0; i < lists.length; i++) {
        if (lists[i].itemGroup == JSON.stringify(chooseGroup)) {
          res = lists[i];
          break;
        }
      }
      console.log("_getCurrChooseInfo", {
        ...this._goods,
        ...res
      });
      this.setData({
        chooseProInfo: {
          ...this._goods,
          ...res
        }
      })
    },
    //检查是否开启限时购
    checkIsFlashSales(data, companyShare) {
      if (!data || companyShare.companyAuth.enableFlashSalesModule != 1) {
        return false;
      }
      if (!data.sales) {
        return false;
      }
      let nt = new Date().getTime();
      let st = utils.getTimeByDate(data.sales.startTime);
      let et = utils.getTimeByDate(data.sales.endTime);
      if (nt >= st && nt < et && data.sales.status > -1) {
        return true;
      }
      return false;
    },
    //检查是否正在拼团中
    checkIsInCollage(data, companyShare) {
      if (!data || companyShare.companyAuth.enableCollage != 1) {
        return false;
      }
      let nt = new Date().getTime();
      let st = utils.getTimeByDate(data.startTime);
      let et = utils.getTimeByDate(data.endTime);
      //当前时间在开始时间和结束时间之间并且拼团状态>-1
      if (nt >= st && nt < et && data.status > -1) {
        return true;
      }
      if (nt < st && data.status == 1) {
        return true;
      }
      return false;
    },
    _getSpecList(data1, data2) {
      let res = [];
      data1.forEach(item1 => {
        data2.forEach(item2 => {
          if (item2.gsId == item1.gsId && item2.mgId == item1.mgId) {
            Array.isArray(item1.child) ? item1.child.push(item2) : item1.child = [item2]
          }
        })
        res.push(item1);
      });
      console.log("_getSpecList", res)
      return res;
    },
    resetSpecList(oldList) {
      if (!this._specInfoList.length) {
        return [];
      }
      let firstSpec = this.curSelectSpec[0];
      for (let item of oldList) {
        let hasStock = this._specInfoList.some(i => i.itemGroup.includes(item.gsiId) && i.stock > 0);
        let groupId = `[${firstSpec},${item.gsiId}]`;
        let curSelectItem = this._specInfoList.find(i => i.itemGroup == groupId);
        hasStock = curSelectItem ? curSelectItem.stock > 0 : hasStock;
        item.hasStock = hasStock;
      }
      return oldList;
    },

  }
})
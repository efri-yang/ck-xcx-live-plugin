import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    qrcodeUrl: {
      type: String,
      default: ""
    },
    joinLiveStatus: {
      type: Number,
      default: 0
    },
    moduleType: {
      type: Number,
      default: 1
    },
    detailId: {
      type: Number,
      default: 0
    },
    socialRoomId: {
      type: Number,
      default: 0
    },
    srmId: {
      type: Number,
      default: 0
    },
    roles: {
      type: Object,
      default: {}
    },
    pathQuety:{
      type: String,
      default: ""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    previewImg() {
      wx.previewImage({
        current: this.data.qrcodeUrl, // 当前显示图片的http链接
        urls: [this.data.qrcodeUrl] // 需要预览的图片http链接列表
      })
    },
    onClose(){
      this.setData({
        isShow:false
      })
    },
    rejectLm() {
      let sendData={
        srmId: this.data.srmId,
        status: 3,
        moduleType: this.data.moduleType,
      }
      if(this.data.detailId){
        sendData.detailId =this.data.detailId;
      }
      utils.post(`appletLiveVideo/setJoinLiveStatus/${this.data.socialRoomId}`,sendData).then((res)=>{
        this.setData({
          isShow:false
        });
        this.triggerEvent("updateLmStatus",res.status)
      });
    }
  }
})
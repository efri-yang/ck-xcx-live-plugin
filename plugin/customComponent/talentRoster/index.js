// customComponent/talentRoster/index.ts
const LIMIT = 30;
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    roles: {
      type: Object,
      value: {}
    },
    isShowRealName: { //是否显示真名
      type: Boolean,
      value: false
    },
    isHideUserName: { //是否隐藏昵称
      type: Boolean,
      default: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    tabIndex: 1,
    listData: [],
    loadStatus:""
  },

  lifetimes: {
    created(){
      this._page = 1;
      this._pageSize=30;
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onScrollToLower(){
      if (this.data.loadStatus == "loading" || this.data.loadStatus == "end" || this.data.loadStatus == "none") return;
      this._page += 1;
      this._getRosterList();
    },
    changeTab(e) {
      let idx = e.currentTarget.dataset.index;
      if (idx == this.data.tabIndex) return;
      this.setData({
        tabIndex:idx,
        listData:[]
      },()=>{
        this._getRosterList();
      })
    },
    onOpen() {
      this._page = 1;
      this.setData({
        listData: []
      })
      if (this.data.activityInfo) {
        this._getRosterList();
      } else {
        this._getActivityInfo().then(() => {
          this._getRosterList();
        })
      }
    },
    _getRosterList() {
      return new Promise((resolve, reject) => {
        this.setData({
          loadStatus: "loading",
        });
        let currTabIndex = this.data.tabIndex;
        utils.get(`live/invite/getWinnerList/${this.data.socialRoomId}}`,{
          limit:LIMIT,
          page:this._page,
          rankType:this.data.tabIndex
        },false).then(res => {
          let resData = res.list ?  res.list.data : [];
          if (currTabIndex != this.data.tabIndex) return;
          //设置加载状态
          if (resData.length == 0 && this._page == 1) {
            this.setData({
              loadStatus: "none",
            })
          } else if ((!resData.length && this._page != 1) || (this._page == 1 && resData.length < this._pageSize)) {
            this.setData({
              loadStatus: "end"
            })
          } else {
            this.setData({
              loadStatus: ""
            })
          }
          this.setData({
            listData: this.data.listData.concat(res.list ? res.list.data:[])
          },()=>{
            console.log("cccc",this.data.listData)
          })
        })
      })
    },
    _getActivityInfo() {
      return new Promise((resolve, reject) => {
        utils.get(`live/invite/getActivityInfo/${this.data.socialRoomId}`).then((res) => {
          let activityInfo = Array.isArray(res) ? {} : res;
          let tabIndex = 1;
          if (!res.isRankReward && res.isNumReward) {
            tabIndex = 2;
          }
          this.setData({
            tabIndex: tabIndex,
            activityInfo: activityInfo
          }, () => {
            resolve();
          })
        }).catch(() => {
          reject();
        })
      })
    },
  }
})
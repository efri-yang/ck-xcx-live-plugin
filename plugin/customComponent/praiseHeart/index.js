import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    queueList:[],
    mapNumber: {
      0: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco1.png",
      1: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco2.png",
      2: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco3.png",
      3: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco4.png",
      4: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco5.png",
      5: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco6.png",
      6: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco7.png",
      7: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco8.png",
      8: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco9.png",
      9: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco10.png",
      10: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco11.png",
      11: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco12.png",
    },
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 添加提醒消息到队列
    public_addPraise() {
      let msg = {
        time:Date.now(),
        src:this.data.mapNumber[Math.floor(Math.random() * 11)]
      }
      this.data.queueList.push(msg);
      this.setData({
        queueList:this.data.queueList
      })
      
      // console.log('333333333333',this.data.queueList)
    },
    afterQueueMessageLeave() {
      this.data.queueList.shift();
      this.setData({
        queueList:this.data.queueList
      })
      // console.log('444444',this.data.queueList)
    }
  }
})
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    nickName:{
      type:String,
      value:"",
      observer(newVal){
        this.setData({
          inputVal:newVal
        })
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    inputVal:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    cardNameInput(e){
      this.setData({
        inputVal: e.detail.value
      })
    },
    onConfirm(){
      console.log("onConfirm");
      let text = this.data.inputVal.replace(/(^\s*)|(\s*$)/g, "");
      if (text!= ''){
        utils.post('social/setNickname',{
          socialRoomId:this.data.socialRoomId,
          nickname:text
        }).then(()=>{
          this.setData({
            isShow:false
          });
          wx.showToast({
            title: '修改成功',
            icon:"success"
          })
          this.triggerEvent("updateBusinessCard",text);
        })
      }else{
        wx.showToast({
          title: '昵称不能为空',
          icon:"error"
        })
      }
    },
    onCancel(){
      this.setData({
        inputVal:this.data.nickName
      })
    }
  }
})

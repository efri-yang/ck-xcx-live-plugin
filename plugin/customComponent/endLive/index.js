import utils from '../../utils/util';
import IM from '../../utils/im';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    liveId:{
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    detailId:{
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    liveType:{
      type: Number,
      optionalTypes: [String],
      default: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    inputVal:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    endLiveInput(e){
      this.setData({
        inputVal: e.detail.value
      })
    },
    onConfirm(){
      if (this.data.inputVal != '结束直播' && this.data.inputVal != 'end live') {
        wx.showToast({
          title: '请输入结束直播',
          icon:"none"
        })
        return;
      }
      if(this.data.detailId){
        utils.post(`appletLivePersonal/endLive/${this.data.liveId}`,{
          detailId: this.data.detailId,
          keyword: '结束直播'
        }).then(()=>{
          IM.sendCustomMessage({
            ...IM.options.ctx._imSendDefaultData,
            msgType: 24
          });
          this.triggerEvent("endLive");
          this.setData({
            isShow:false
          })
        })
      }else{
        utils.get(`liveFlow/endVoiceLive/${this.data.liveId}`).then(()=>{
          if(this.data.liveType !=3 && this.data.liveType != 4){
            utils.get(`liveFlow/getViewRecent/${this.data.liveId}`).then(()=>{
              IM.sendCustomMessage({
                ...IM.options.ctx._imSendDefaultData,
                msgType: 24
              });
              this.triggerEvent("endLive");
              this.setData({
                isShow:false
              })
            })
          } else {
            IM.sendCustomMessage({
              ...IM.options.ctx._imSendDefaultData,
              msgType: 24
            });
          }
        })
      }
      
    },
    onCancel(){
      this.setData({
        inputVal:""
      })  
    }
  }
})

import utils from '../../utils/util';
const PAGE_SIZE = 50;
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    liveId: {
      type: Number,
      optionalTypes: [String],
      value: 0
    },
    detailId: {
      type: Number,
      optionalTypes: [String],
      value: 0
    },
    moduleType: {
      type: Number,
      optionalTypes: [String],
      value: 1, //1公开课  2直播间 
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    rosterList: [],
    rosterTotal: 0,
    loadStatus: "",
  },

  /**
   * 组件的方法列表
   */
  methods: {
    show(id) {
      this._id = id;
      this._page = 1;
      this.setData({
        rosterTotal: 0,
        rosterList: [],
        isShow: true
      })
    },
    open() {
      this.getList();
    },
    getList() {
      let url = `live/lottery/getWinnerList/${this.data.liveId}/${this._id}`;
      let sendData = {
        page: this._page,
        limit: PAGE_SIZE
      }
      if (this.data.detailId) {
        sendData.detailId = this.data.detailId;
      }
      this.setData({
        loadStatus: "loading"
      })
      utils.get(url,sendData,false).then(res => {
        //设置加载状态
        res.data = res.data || [];
        if (res.data.length == 0 && this._page == 1) {
          this.setData({
            loadStatus: "none",
          })
        } else if ((!res.data.length && this._page != 1) || (this._page == 1 && res.data.length < PAGE_SIZE)) {
          this.setData({
            loadStatus: "end"
          })
        } else {
          this.setData({
            loadStatus: ""
          })
        }
        this.setData({
          rosterList: this.data.rosterList.concat(res.data),
          rosterTotal: res.total || 0
        })
      }).catch(()=>{
        this.setData({
          loadStatus: ""
        })
      })
    },
    onScrollToLower() {
      if (this.data.loadStatus == 'loading' || this.data.loadStatus == 'end') return;
      this._page++;
      this.data.loadStatus ='loading'
      this.setData({
        loadStatus: this.data.loadStatus
      }, () => {
        this.getList();
      });
    }
  }
})
// plugin/customComponent/guidanceGroupQrCode/index.ts
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    info: {
      type: Object,
      default: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    previewImg(){
      wx.previewImage({
        current: this.data.info.guideQrcode, // 当前显示图片的http链接
        urls: [this.data.info.guideQrcode],
      })
    },
    open() {
      this.setData({
        isShow: true
      })
    },
    close() {
      this.setData({
        isShow: false
      })
    }
  }
})
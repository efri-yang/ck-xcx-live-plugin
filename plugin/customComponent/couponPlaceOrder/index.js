// plugin/customComponent/couponPlaceOrder/index.ts
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow:{
      type:Boolean,
      value:false
    },
    lists:{
      type:Array,
      value:[]
    },
    chooseId:{
      type:Number,
      value:-1
    }

  },

  lifetimes: {
    attached: function () {
     
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行

    }
  },

  /**
   * 组件的初始数据
   */
  data: {
   
  },

  /**
   * 组件的方法列表
   */
  methods: {
    chooseCoupon(e){
      let data=e.currentTarget.dataset.item;
      let triggerData= null
      console.log("data",data);
      if(data.rcdId != this.data.chooseId){
        triggerData=data
      }
      this.triggerEvent("chooseCoupon",triggerData);
      this.setData({
        isShow:false
      })
    }
  }
})

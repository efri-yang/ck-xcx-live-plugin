import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    liveId: { //打赏的表示专门独立直播间用
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    detailId: { //打赏的表示专门独立直播间用
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    liveStatus: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },

  },

  lifetimes: {
    attached: function () {
      this.getWelfareInfo();
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    statusTextDict: { //状态文本字典
      1: '已领取',
      2: '领取',
      3: '请稍等',
      '-1': '已抢光',
      '-2': '已失效'
    }, //状态文本字典
    isShow: false,
    info: {},
    currDc: {}, //当前倒计时对象
  },

  /**
   * 组件的方法列表
   */
  methods: {
    show() {
      setTimeout(()=>{
        this.setData({
          isShow: true
        });
      },500)
      
      this._remainCount = 0;
    },
    onClose(){
      this.triggerEvent("recordWatchCount");
    },
    //领取福利
    getWelfare(e) {
      let index = e.currentTarget.dataset.index;
      let item = e.currentTarget.dataset.item;
      if (item.status != 2) return;
      this.setData({
        isShow: false
      });
      if (item.ty == 2) {
        //实体商品领取
        let data = {
          liveId: this.data.liveId,
          prizeId: this.data.info.lwId * 10 + Number(item.sort),
        }
        if (this.data.detailId) {
          data.detailId = this.data.detailId;
        }
        utils.get('live/liveWeal/getWealPrizeInfo', data, false).then(res => {
          res.prizeId = data.prizeId;
          res.activityType = 128; //福利下单的类型是128
          //独立直播间的比较特殊
          if (this.data.detailId) {
            res.rId = this.data.detailId;
          }
          this.triggerEvent('openWelfarePrizeDetail', res)
        })
      } else {
        //非实体商品领取(直接下放)
        item.activityType = 128; //福利下单的类型是64
        let data = {
          liveId: this.data.liveId,
          lwId: this.data.info.lwId,
          sort: item.sort,
        }
        if (this.data.detailId) {
          data.detailId = this.data.detailId;
        }
        utils.post('live/liveWeal/claimLiveWeal', data).then(res => {
          let liveWeals = this.data.info.liveWeals;
          item.status = 1;
          item.isGet = true;
          liveWeals[index] = item;
          let welfareEnableNum = liveWeals.filter(i => i.status == 2).length;
          this.triggerEvent("setWelfareEnableNum", welfareEnableNum)
          this.setData({
            'info.liveWeals': liveWeals
          });
          //福利非实体中奖是后端下放的，所以不需要领取，无需传递 奖品的id
          this.triggerEvent('openWelfarePrizeDetail', item)
        })
      }

    },

    getWelfareInfo() {
      let sendData = {
        liveId: this.data.liveId
      };
      if (this.data.detailId) {
        sendData.detailId = this.data.detailId;
      }
      utils.get('live/liveWeal/getLiveWeals', sendData, false).then((res) => {
        res.watchTimes = res.watchTimes * 1;
        res.liveWeals.forEach(item => {
          item.status = item.isGet ? 1 : ((res.watchTimes > item.mm * 60) ? 2 : 3);
          if (!item.isGet && (res.watchTimes > item.mm * 60)) {
            //未领取的时候判断库存
            if (item.ty == 1 && item.prodType == 92 && item.type) {
              //虚拟商品和优惠券
              if (!item.leftNum) {
                //没库存
                item.status = -1;
              } else if ([3, -5, 4, 5].includes(item.couponStatus)) {
                //抢光了
                item.status = -2;
              }
            } else if (item.ty == 2 && item.prodType == 12 && !item.stock) {
              //实体商库存
              item.status = -1;
            } else if (item.ty == 4 && !item.isStore) {
              //红包库存为零
              item.status = -1;
            }
          }
        });
        let welfareEnableNum = res.liveWeals.filter(i => i.status == 2).length;
        this.triggerEvent("setWelfareEnableNum", welfareEnableNum);

        let currIndex = res.liveWeals.findIndex(item => res.watchTimes < item.mm * 60);
        console.log("currIndex",currIndex)
        if (currIndex != -1 && this.data.liveStatus >= 1) {
          let seconds = (res.liveWeals[currIndex].mm * 60 - res.watchTimes);
          console.log("seconds",seconds)
          this.setData({
            currDc:{
              idx:currIndex,
              count:seconds,
              timeStr:utils.dtFormat(seconds * 1000,"mm:ss")
            }
          },()=>{
            this.downCount();
          })
        }

        this.setData({
          info: res
        });
      });
    },
    downCount() {
      clearTimeout(this._dcTimer);
      let currDc = this.data.currDc;
      if (currDc.count > 1) {
        currDc.count--;
        this.setData({
          'currDc.count':currDc.count,
          'currDc.timeStr':utils.dtFormat(currDc.count * 1000,"mm:ss")
        })
        this._dcTimer = setTimeout(() => {
          this.downCount();
        }, 1000)
        if(currDc.count==5){
          this.triggerEvent("recordWatchCount",8);
        }
      }else{
        this.triggerEvent("recordWatchCount");
        let currIndex = this.data.currDc.idx + 1;
        if(currIndex < this.data.info.liveWeals.length){
          let seconds = (this.data.info.liveWeals[currIndex].mm * 60 - this.data.info.liveWeals[this.data.currDc.idx].mm * 60);
          this.setData({
            [`info.liveWeals[${currIndex-1}].status`]:2,
            currDc:{
              idx:currIndex,
              count:seconds,
              timeStr:utils.dtFormat(seconds * 1000,"mm:ss")
            }
          },()=>{
            this.downCount();
          })
        }else{
          this.setData({
            [`info.liveWeals[${this.data.currDc.idx}].status`]:2,
            currDc:{}
          })
        }
      }
    }
  }
})
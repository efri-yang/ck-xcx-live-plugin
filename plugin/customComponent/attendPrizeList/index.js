import utils from '../../utils/util';
import config from '../../config';
const PAGE_SIZE = 30;
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    liveId: {
      type: Number,
      optionalTypes: [String],
      value: 0
    },
    detailId: {
      type: Number,
      optionalTypes: [String],
      value: 0
    },
    moduleType: {
      type: Number,
      optionalTypes: [String],
      value: 1, //1公开课  2直播间 
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    acceptData: {},
    lists: [],
    tabList: [{
      tit: "抽奖奖品",
      type: 64
    },{
      tit: "福利奖品",
      type: 128
    }],
    tabIndex:0,
    prod_img_dict: config.defaultProdImgDict,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // type：活动类型
    show(data) {
      console.log("data", data);
      if(data && data.type){
        let tabIndex = this.data.tabList.findIndex(item=>item.type==data.type);
        if(tabIndex !=-1){
          this.data.tabIndex = tabIndex
        }
      }else{
        this.data.tabIndex = 0;
      }
      this._page = 1;
      this.setData({
        lists: [],
        acceptData: data,
        tabIndex:this.data.tabIndex
      }, () => {
        this.setData({
          isShow: true
        })
      })
    },
    refresh() {
      this._page = 1;
      this.setData({
        lists: []
      }, () => {
        this.getPrizeList();
      })
    },
    open() {
      this.getPrizeList();
    },
    switchTab(e){
      let dataset=e.currentTarget.dataset;
      if(dataset.index== this.data.tabIndex) return;
      this.setData({
        tabIndex:dataset.index,
        lists:[],
      },()=>{
        this.getPrizeList();
      })
    },
    //领奖
    getActivityPrize(e) {
      let info = e.currentTarget.dataset.item;
      console.log("info", info)
      this.triggerEvent("getActivityPrize", {
        mgId: info.prodId,
        quantity: 1,
        query: {
          fromType: this.data.acceptData.type,
          rId: info.rId,
          prizeId: info.prizeId
        }
      })
    },
    getPrizeList() {
      let url = "";
      let tabIndex = this.data.tabIndex;
      let activityType = this.data.tabList[tabIndex].type;
      let sendData= {};
      if (activityType == 64) {
        //抽奖
        url = `live/lottery/getMyPrizeList/${this.data.liveId}`;
      } else if(activityType== 128){
        url = 'live/liveWeal/myLiveWeals';
        sendData.liveId = this.data.liveId;
        if(this.data.detailId){
          sendData.detailId = this.data.detailId;
        }
      }
      utils.get(url,sendData).then(res => {
        if (res.length == 0 && this._page == 1) {
          this.setData({
            loadStatus: "none",
          })
        } else if ((!res.length && this._page != 1) || (this._page == 1 && res.length < PAGE_SIZE)) {
          this.setData({
            loadStatus: "end"
          })
        } else {
          this.setData({
            loadStatus: ""
          })
        }
        this.setData({
          lists: this.data.lists.concat(res)
        })
      })
    }
  }
})
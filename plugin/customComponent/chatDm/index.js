Component({

  /**
   * 组件的属性列表
   */
  properties: {
    chatList: {
      type: Array,
      value: []
    },
    jiguangUserName: {
      type: Number,
      optionalTypes: [String],
      value: 0,
    },
    welcomeMessage:{
      type:Array,
      value:[]
    }
  },


  /**
   * 组件的初始数据
   */
  data: {
    audioPlayItem: {
      socialRoomMessageId: 0,
      status: 0, //0初始状态 1加载中 2播放中 3暂停
    },
    sourceMapType: {
      31: "视频",
      32: "音频",
      33: "图文",
      34: "专栏",
      35: "测评",
      36: "活动",
      37: "打卡"
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    previewImage(e) {
      let item = e.currentTarget.dataset.item;
      wx.previewImage({
        urls: [item.content],
        current: item.content
      })
    },
    audioTogglePlay(e) {
      if (!this._audioCtx) this._createInnerAudioCtx();
      let item = e.currentTarget.dataset.item;
      if (this.data.audioPlayItem.socialRoomMessageId != item.socialRoomMessageId) {
        this.setData({
          audioPlayItem: {
            socialRoomMessageId: item.socialRoomMessageId,
            status: 0
          }
        });
        this._audioCtx.src = item.content;
      } else {
        if (this._audioCtx.paused) {
          this._audioCtx.play();
        } else {
          this._audioCtx.pause();
        }
      }
    },
    //创建音频实例
    _createInnerAudioCtx() {
      this._audioCtx = wx.createInnerAudioContext({
        useWebAudioImplement: false
      });
      this._audioCtx.autoPlay = true;
      this._audioCtx.onStop(() => {
        console.log('onStop');
        this.setData({
          'audioPlayItem.status': 0
        })
      })
      this._audioCtx.onWaiting((res) => {
        console.log("onWaiting", res);
        this.setData({
          'audioPlayItem.status': 1
        });
      })
      this._audioCtx.onPlay(() => {
        console.log('onPlay');
        this.setData({
          'audioPlayItem.status': 2
        });
      })

      this._audioCtx.onPause(() => {
        console.log('onPause');
        this.setData({
          'audioPlayItem.status': 3
        })
      })

      this._audioCtx.onError((res) => {
        console.log("onError", res);
        this.setData({
          'audioPlayItem.status': 0
        })
      });
      this._audioCtx.onCanplay(() => {
        console.log("onCanplay", this._audioCtx.autoPlay, this._audioCtx.paused)
        if (this._audioCtx.autoPlay) {
          this._audioCtx.play();
        }
        this.setData({
          'audioPlayItem.status': 2
        })
      });
      this._audioCtx.onEnded((res) => {
        console.log("onEnded", res);
        this.setData({
          'audioPlayItem.status': 0
        })
      })
    },
  }
})
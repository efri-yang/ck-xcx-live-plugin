import config from '../../config';
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    detailId:{
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    info: {
      type: Object,
      value: {
        // amount: "0.00",
        // avatar: "https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJLg6o4tkvX4nEgAYnNgYhh5PzxFFda2l8WOVMrs548jwiaJFicxolgOUgPYav4wTGDcibZEP8aE2tGw/132",
        // content: "1018无门槛指定类型9",
        // createdAt: "15:03",
        // dynamicName: "散崖",
        // endTime: null,
        // imgUrl: null,
        // jiguangUserName: 322255368,
        // jimRoomId: "@TGS#a6JH2DDJ2",
        // msgType: 70,
        // pId: null,
        // payType: 1,
        // playersPrice: null,
        // price: "5.00",
        // prodPrice: "5.00",
        // realName: "散崖",
        // redeemType: 27,
        // relationId: 25429,
        // relationType: 7,
        // roomId: "25745",
        // socialRoomMessageId: 1617038,
        // sort: 0,
        // startTime: null,
        // subContent: null,
        // subJiguangUserName: null,
        // subMsgId: 25429,
        // subName: null,
        // subProdId: null,
        // subProdType: 92,
        // subRole: 0,
        // subUserRealName: null,
        // tag: "讲师",
        // tagArr: "",
        // type: 2,
        // userName: "散崖",
        // userRealName: "散崖真名",
        // userRole: 6,
        // validPeriod: 1,
        // validStatus: 1,
        // version: "20210518",
        // vipInfo: null,
        // voiceDuration: 1
      },
      observer(){
        if(this._dcTimer){
          clearTimeout(this._dcTimer);
          this.setData({
            timeCount: 10
          },()=>{
            this._downCount();
          });
        }
      }
    }
  },

  lifetimes: {
    attached: function () {
      this.setData({
        isIos: utils.isIos()
      })
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行

    }
  },





  /**
   * 组件的初始数据
   */
  data: {
    timeCount: 10,
    prodMapType: config.prodMapType,
    prodCourseType: config.prodCourseType,
    relationTypeMap: config.relationTypeMap,
    isIos:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getCoupon() {
      let info = this.data.info;
      if (this._isRequesting) return;
      utils.getBindPhoneSetting(config.ckFrom.coupon).then((res) => {
        if (res.getBind) {
          wx.showToast({
            title: '请绑定手机号码',
          });
          this.triggerEvent("bindPhone")
          this._isRequesting = false;
        } else {
            let url = '',
            data = {
              source: 13,
              SocialRoomId: this.data.socialRoomId
            };
          if(this.data.detailId){
            data.detailId = this.data.detailId;
          }
          if (info.prodType == 200) {
            url = `coupon/collectJWCoupon`;
            data.redeemCodeId = info.relationId;
          } else {
            url = `redeemCodes/collectRedeemCode/${info.relationId}`;
          }
          utils.post(url, data).then(res => {
            this._isRequesting = false;
            if (res.status == 1) {
              wx.showToast({
                title: '领取成功，请在“个人中心>优惠券”中查看',
                icon: "none"
              })
            } else {
              wx.showToast({
                title: res.detail ? res.detail : res.msg,
                icon: "none"
              })
            }
            this.onClose();
          }).catch(() => {
            this._isRequesting = false;
          })
        }
      }).catch(() => {
        this._isRequesting = false;
      })
    },
    onOpen() {
      clearTimeout(this._dcTimer);
      this.setData({
        timeCount: 10
      },()=>{
        this._downCount();
      });
    },
    onClose() {
      clearTimeout(this._dcTimer);
      this._dcTimer=null;
      this.setData({
        timeCount: 10,
        isShow: false
      });
    },
    _downCount() {
      if (this.data.timeCount < 1) {
        this.onClose();
      } else {
        this.setData({
          timeCount: this.data.timeCount
        });
        this.data.timeCount--;
        this._dcTimer = setTimeout(() => {
          this._downCount()
        }, 1000)
      }
    },
  }
})
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    moduleType: {
      type: Number,
      default: 1
    },
    detailId: {
      type: Number,
      default: 0
    },
    socialRoomId: {
      type: Number,
      default: 0
    },
    srmId: {
      type: Number,
      default: 0
    },
    timeCount: {
      type: Number,
      default: 0
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onOpen(){
      this._startDownCount();
    },
    onConfirm(){
      if (this._isRequesting) return;
      this._isRequesting = true;
      let sendData={
        moduleType:this.data.moduleType,
        srmId: this.data.srmId,
        status: 1,
      }
      if(this.data.detailId){
        sendData.detailId=this.data.detailId;
      }
      utils.post(`appletLiveVideo/setJoinLiveStatus/${this.data.socialRoomId}`,sendData,false).then(()=>{
         this._isRequesting = false;
         wx.showToast({
           title: '已成功提交申请！',
           icon:"none"
         })
         this.setData({
           isShow:false
         })
      }).catch(()=>{
        this._isRequesting = false;
      })
    },
    onClose(){
      clearTimeout(this._timer);
    },
    _startDownCount: function () {
      if (this.data.timeCount > 1) {
        this.data.timeCount--;
        this.setData({
          timeCount:this.data.timeCount
        })
        this._timer = setTimeout(()=>{
          this._startDownCount()
        }, 1000)
      } else {
        this.setData({
          isShow:false
        }) 
      }
    }
  }
})

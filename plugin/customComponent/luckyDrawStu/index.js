import config from '../../config';
import utils from '../../utils/util';
import IM from '../../utils/im';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    liveId: {
      type: Number,
      value: 0
    },
    isTaboo:{ //是否禁言（群禁言或者个人禁言）
      type:Boolean,
      value:false,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    info: {},
    prod_img_dict: config.defaultProdImgDict,
    downCountTime: "", //倒计时显示
  },

  /**
   * 组件的方法列表
   */
  methods: {
    show(id) {
      this._lotteryId = id;
      this.setData({
        isShow: true
      });
    },
    hide(){
      this.setData({
        isShow:false
      })
    },
    open() {
      utils.get(`live/lottery/show/${this.data.liveId}/${this._lotteryId}`).then(res => {
        this.data.downCountTime = utils.dtFormat(res.lotteryDrawAt * 1000,'hh:mm:ss');
        this._dcTimer && clearTimeout(this._dcTimer);
        this.data.info = res;
        this._lotteryDownCount();
        this.setData({
          downCountTime:this.data.downCountTime,
          info: this.data.info
        });
        this.triggerEvent("luckyDrawChange",{
          lotteryDrawAt: res.lotteryDrawAt,
          lotteryId: res.lotteryId,
          lotteryJoin: res.hasJoin
        });
      })
    },
    joinLottery(){
      let info = this.data.info;
      if(info.joinType == 7 && this.data.isTaboo){
        wx.showToast({
          title: '当前为禁言模式,无法参与该活动!',
          icon:'none',
          duration:2500
        })
        return;
      }
      utils.post(`live/lottery/join/${this.data.liveId}/${this._lotteryId}`).then((res)=>{
        if (res.data == 1) {
          info.hasJoin = 1;
          if(info.joinType == 7) {
            //评论参与
            IM.sendCustomMessage({
              ...IM.options.ctx._imSendDefaultData,
              msgType: 1,
              content:info.joinValue
            })
            IM.options.ctx.saveMsgToServer({
              msgType: 1,
              content:info.joinValue
            }).then((res) => {
              IM.options.ctx.addLocalChatMsg(res);
            })
          }
          wx.showToast({
            title: '参与成功!',
            icon:"none",
            duration:2500
          })
          this.setData({
            info:info
          });
          this.triggerEvent("luckyDrawChange",{
            lotteryDrawAt: info.lotteryDrawAt,
            lotteryId: info.lotteryId,
            lotteryJoin: info.hasJoin
          });
          setTimeout(()=>{
            this.setData({
              isShow:false
            })  
          },2000)
        }else{
          if(info.joinType == 8 || info.joinType == 9){
            wx.showModal({
              title: '提示',
              content: res.msg,
              showCancel:false,
              confirmText:"我知道了"
            })
          }else{
            wx.showToast({
              title: res.msg,
              icon:'none',
              duration:2500
            })
          }
        }
      })
    },

    openInvitionCard(){
      this.hide();
      this.triggerEvent("openInvitionCard")
    },
    _lotteryDownCount: function () {
      if (this.data.info.lotteryDrawAt < 1) {
        clearTimeout(this._dcTimer);
        this._dcTimer = null;
        this.setData({
          isShow: false
        })
      } else {
        this.data.info.lotteryDrawAt--;
        this.data.downCountTime = utils.dtFormat(this.data.info.lotteryDrawAt * 1000,'hh:mm:ss');
        this.setData({
          downCountTime:this.data.downCountTime
        })
        this._dcTimer = setTimeout(() => {
          this._lotteryDownCount()
        }, 1000)
      }
    },
  }
})
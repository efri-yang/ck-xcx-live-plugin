import utils from '../../utils/util';
import IM from '../../utils/im';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    liveId: {
      type: Number,
      default: 0
    },
    detailId: {
      type: Number,
      default: 0
    },
    moduleType: {
      type: Number,
      default: 0
    },
    liveStatus:{
      type: Number,
      default: 1
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShowList: false, //是否显示抽奖列表
    isShowSend: false, //是否显示发送抽奖表单
    isShowEnd: false, //是否显示立即开奖
    lotteryList: [], //抽奖列表
    lotteryDetail: {}, //抽奖的信息
    currLottery: {},
    prText: {
      '1': '中奖机会均等',
      '2': 'VIP/SVIP/服务商中奖几率大',
      '3': '普通学员中奖几率大',
      '4': '分享直播的中奖几率大',
      '5': '打赏后的中奖几率大'
  },
    lotteryDuration: 60,
    currLotteryDcTime: 0, //当前抽奖的开奖倒计时  
  },

  /**
   * 组件的方法列表
   */
  methods: {
    showList() {
      this.setData({
        isShowList: true
      })
    },
    openLotteryList() {
      utils.get(`appletLottery/getLotteryList/${this.data.liveId}`, {
        moduleType: this.data.moduleType,
        detailId: this.data.detailId
      }, false).then((res) => {
        this.setData({
          currLottery: res.filter(item => item.status == 1)[0] || {},
          lotteryList: res,
        })
      })
    },
    openRoster(e) {
      let data = e.currentTarget.dataset.item;
      console.log("data", data);
      this.triggerEvent("openRoster", {
        id: data.lotteryId
      })
    },
    confirmOpen(e) {
      let id = e.currentTarget.dataset.id;
      this.setData({
        lotteryDuration:60,
        isShowSend: true,
      });
      this._getLotteryDetail(id);
    },
    //发送抽奖
    onBeginLottery() {
      if (this._isBeginLotteryRequesting) return;
      let lotteryDuration = this.data.lotteryDuration;
      if (lotteryDuration < 10 || lotteryDuration > 10000) {
        wx.showToast({
          title: '开奖设置时长需要在10~10000之间'
        });
        return;
      }
      this._isBeginLotteryRequesting = true;
      utils.post(`appletLottery/startLottery/${this.data.lotteryDetail.lotteryId}`, {
        liveId: this.data.liveId,
        drawSeconds: lotteryDuration,
        detailId: this.data.detailId
      }, true).then((res) => {
        this._isBeginLotteryRequesting = false;
        //显示抽奖入口
        this.triggerEvent("luckyDrawChange", {
          lotteryId: this.data.lotteryDetail.lotteryId,
          lotteryDrawAt: lotteryDuration
        });
        //发送自动以消息
        IM.sendCustomMessage({
          ...IM.options.ctx._imSendDefaultData,
          msgType: 271,
          content: this.data.lotteryDetail.lotteryId
        });

        this.setData({
          isShowSend: false,
          isShowList:false,
        })
        wx.showToast({
          title: '发起成功!',
          icon: "success"
        })

      }).catch(() => {
        this._isBeginLotteryRequesting = false;
      })
    },
    //获取抽奖详情
    _getLotteryDetail: function (id) {
      return new Promise((resolve, reject) => {
        utils.get(`appletLottery/getLotteryDetail/${id}`).then((res) => {
          res.prizes.forEach((item) => {
            if (item.type == 5) {
              //折扣
              item.price = Number(item.price)
            }
            item.tip = Number(item.amount) > 0 ? ('满' + Number(item.amount) + '可用') : '无门槛'
          })
          this.setData({
            lotteryDetail: res,
          }, () => {
            resolve(res);
          })
        }).catch(() => {
          reject();
        })
      })
    },
    //
    lotteryDurationInput: function (e) {
      this.setData({
        lotteryDuration: e.detail.value
      });
    },
    //开始抽奖倒计时
    _lotteryDownCount: function () {
      if (this.data.lotteryDetail.lotteryDrawAt < 1) {
        clearTimeout(this._dcTimer);
        //不限时间，不关闭
        this._dcTimer = null;
        this.setData({
          isShowEnd:false
        });
        this.triggerEvent("luckyDrawChange");
      } else {
        this.data.lotteryDetail.lotteryDrawAt--;
        this.setData({
          currLotteryDcTime: utils.dtFormat(this.data.lotteryDetail.lotteryDrawAt * 1000,'hh:mm:ss')
        })
        this._dcTimer = setTimeout(() => {
          this._lotteryDownCount()
        }, 1000)
      }
    },
    hideDetail(){
      this.setData({
        isShowList: false,
        isShowEnd: false
      })
    },
    showDetail(e) {
      let id = "";
      if (e.currentTarget) {
        id = e.currentTarget.dataset.id;
      } else {
        id = e;
      }
      this.setData({
        isShowList: false,
        isShowEnd: true
      })
      this._getLotteryDetail(id).then(()=>{
        this._lotteryDownCount();
      })
    },
    onEndLottery(e){
      let id = e.currentTarget.dataset.id;
      utils.post(`appletLottery/endLottery/${id}`).then(() => {
        this.setData({
          isShowEnd:false
        })
        //发送自动以消息
        IM.sendCustomMessage({
          ...IM.options.ctx._imSendDefaultData,
          msgType: 272,
          content: id
        });
        this.triggerEvent("luckyDrawChange");
        this.triggerEvent("openRoster", {
          id: id
        })
    })

    }
  }
})
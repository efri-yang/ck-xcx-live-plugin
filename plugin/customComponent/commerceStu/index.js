import utils from '../../utils/util';
import config from '../../config';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    isFullScreen: {
      type: Boolean,
      value: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    detailId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    }
  },

  lifetimes: {
    attached: function () {
      this.setData({
        isIos: utils.isIos()
      })
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行

    }
  },




  /**
   * 组件的初始数据
   */
  data: {
    commerce: {},
    prodMapType: config.prodMapType,
    prodCourseType: config.prodCourseType,
    tabIndex: 1,
    isIos: false,
    scrollTop: 0,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeTab(e) {
      let index = e.currentTarget.dataset.index;
      if (index == this.data.tabIndex) return;
      this.setData({
        scrollTop: 0,
        tabIndex: index
      })
    },
    onOpen() {
      let sendData = {};
      if (this.data.detailId) {
        sendData.detailId = this.data.detailId
      }
      utils.get(`social/getLiveBringGoods/${this.data.socialRoomId}`, sendData, false).then(res => {
        res.couponGoods.forEach(item => {
          if (item.endTime) {
            let nowtime = new Date().getTime();
            let couponTime = new Date(item.endTime.replace(/-/g, '/')).getTime();
            item._isEnd = nowtime >= couponTime ? true : false;
          }
        })
        this.setData({
          commerce: res
        })
      })
    },
    //直接购买还是去引导
    switchBuyGuide(e) {
      let data = e.currentTarget.dataset.item;
      this.triggerEvent("switchBuyGuide",data)
    },
    openCouponBuyGuide() {
      wx.showToast({
        title: '小程序暂不支持购买',
        icon: "none"
      })
    },
    getCoupon(e) {
      let info = e.currentTarget.dataset.item;
      if (this._isRequesting) return;
      utils.getBindPhoneSetting(config.ckFrom.coupon).then((res) => {
        if (res.getBind) {
          wx.showToast({
            title: '请绑定手机号码',
          });
          this.triggerEvent("bindPhone")
          this._isRequesting = false;
        } else {
          let url = '',
            data = {
              source: 13,
              SocialRoomId: this.data.socialRoomId
            };
          if (this.data.detailId) {
            data.detailId = this.data.detailId;
          }
          if (info.prodType == 200) {
            url = `coupon/collectJWCoupon`;
            data.redeemCodeId = info.relationId;
          } else {
            url = `redeemCodes/collectRedeemCode/${info.relationId}`;
          }
          utils.post(url, data).then(res => {
            this._isRequesting = false;
            if (res.status == 1) {
              wx.showToast({
                title: '领取成功，请在“个人中心>优惠券”中查看',
                icon: "none"
              })
            } else {
              wx.showToast({
                title: res.detail ? res.detail : res.msg,
                icon: "none"
              })
            }
            this.onClose();
          }).catch(() => {
            this._isRequesting = false;
          })
        }
      }).catch(() => {
        this._isRequesting = false;
      })
    },
    refreshData() {
      this.setData({
        scrollTop: 0,
      });
      this.onOpen();
    }
  }
})
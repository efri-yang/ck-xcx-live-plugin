// customComponent/talentRank/index.ts
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    roles: {
      type: Object,
      value: {}
    },
    isShowRealName: { //是否显示真名
      type: Boolean,
      value: false
    },
    isHideUserName: { //是否隐藏昵称
      type: Boolean,
      default: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    liveId: {
      type: Number,
      default: 0,
    },
    detailId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    }
  },

  lifetimes: {
    attached: function () {
      // 在组件实例进入页面节点树时执行
      console.log("tank-attached")
      this._page = 1;
      this._pageSize = 30;
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
      console.log("tank-detached")
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShowActivityIntro: false,
    activityIntro: "",
    myInviteInfo: {},
    inviteList: [],
    activityInfo: {},
    tabIndex: 0, // 0都不是 1冲榜活动  2 多邀多得
    isShowRoster: false,
    inviteUserTotal: 0,
    inviteUserList: [],
    inviteUserLoadStatus: "",
    isShowInviteUserDetail: false, //是否显示邀请人数
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //查看邀请人数
    showInviteUserDetail() {
      this.setData({
        isShowInviteUserDetail: true
      })
    },
    hideInviteUserDetail() {
      this.setData({
        isShowInviteUserDetail: false
      })
    },
    //中奖名单
    openTalentRankRoster() {
      this.triggerEvent("openTalentRankRoster")
    },
    openInvitionCard() {
      this.triggerEvent("openInvitionCard")
    },
    //
    openActivityIntro() {
      this.setData({
        isShowActivityIntro: true
      })
    },
    changeTab(e) {
      console.log(e);
      let index = e.currentTarget.dataset.index;
      this.setData({
        tabIndex: index
      })
    },
    inviteUserListOpen() {
      this._page = 1;
      this.setData({
        inviteUserList: []
      }, () => {
        this.getInvlteUserList();
      })
    },
    getInvlteUserList() {
      this.setData({
        inviteUserLoadStatus: "loading",
      });
      let sendData = {
        limit: this._pageSize,
        page: this._page
      }
      if (this.data.detailId) {
        sendData.detailId = this.data.detailId;
      }
      utils.get(`social/getSocialInviteDetail/${this.data.socialRoomId}`, sendData, false).then(res => {
        if (res.data.length == 0 && this._page == 1) {
          this.setData({
            inviteUserLoadStatus: "none",
          })
        } else if ((!res.data.length && this._page != 1) || (this._page == 1 && res.data.length < this._pageSize)) {
          this.setData({
            inviteUserLoadStatus: "end"
          })
        } else {
          this.setData({
            inviteUserLoadStatus: ""
          })
        }
        this.setData({
          inviteUserTotal: res.total,
          inviteUserList: this.data.inviteUserList.concat(res.data ? res.data : [])
        })
      }).catch(()=>{
        this.setData({
          inviteUserLoadStatus: "",
        });
      })
    },
    onScrollToLower(){
      if (this.data.inviteUserLoadStatus == "loading" || this.data.inviteUserLoadStatus == "end" || this.data.inviteUserLoadStatus == "none") return;
      this._page += 1;
      this.getInvlteUserList();
    },
    talentRankOpen() {
      let url = '';
      let sendData = {
        socialId: this.data.socialRoomId
      }
      if (this.data.detailId) {
        url = `live/livePersonal/getDetailInviteList/${this.data.liveId}`;
        sendData.detailId = this.data.detailId
      } else {
        url = 'social/inviteList'
      }
      utils.get(url, sendData, false).then((res) => {
        let myInviteInfo = utils.getDynamicName(res.myRank, this.data.roles, this.data.isShowRealName);
        res.rank.data.forEach(item => {
          item = utils.getDynamicName(item, this.data.roles, this.data.isShowRealName);
          item.dynamicName = utils.getHideName(item.dynamicName, this.data.isHideUserName);
        });
        this.setData({
          myInviteInfo: res.myRank,
          inviteList: res.rank.data
        })
      });
      utils.get(`live/invite/getActivityInfo/${this.data.socialRoomId}`).then((res) => {
        let activityInfo = Array.isArray(res) ? {} : res;
        let tabIndex = 0;
        if (activityInfo.actId && activityInfo.isRankReward) {
          //开启榜单奖励
          tabIndex = 1;
        } else if (activityInfo.actId && activityInfo.isNumReward) {
          //开启人数成就奖励
          tabIndex = 2
        } else {
          //都没开启
          tabIndex = 0
        }
        this.setData({
          tabIndex: tabIndex,
          activityInfo: activityInfo
        })
      })
    },
    activityIntroOpen() {
      console.log(this.data.activityInfo.descPath)
      if (this.data.activityIntro) return;
      if (this.data.activityInfo.descPath) {
        wx.request({
          url: this.data.activityInfo.descPath,
          method: "GET",
          success: (res) => {
            this.setData({
              activityIntro: res.data
            })
          }
        })
      } else {
        this.setData({
          activityIntro: 'default'
        })
      }
    }
  }
})
import IM from '../../utils/im';
import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShow: {
      type: Boolean,
      value: false
    },
    socialRoomId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    liveId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    },
    detailId: {
      type: Number,
      optionalTypes: [String],
      default: 0
    }
  },
  lifetimes: {
    created() {
      this._viewCount = 0;
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    countArr:[],
  },

  /**
   * 组件的方法列表
   */
  methods: {
    animationend(e){
      this.data.countArr.shift();
      this.setData({
        countArr:this.data.countArr
      })
      clearTimeout(this._animationendTimer);
      this._animationendTimer=setTimeout(()=>{
        this.setData({
          countArr:[]
        })
      },1000)
    },
    brushViewCount() {
      this._viewCount++;
      this.data.countArr.push(this._viewCount);
      this.setData({
        countArr:this.data.countArr
      })
      if (!this._timer) {
        this._timer = setTimeout(() => {
          let sendData={
            num: this._viewCount,
            socialRoomId: this.data.socialRoomId,
            id: this.data.liveId
          }
          if(this.data.detailId){
            sendData.detailId=this.data.detailId
          }
          utils.post('social/flushWatchNum',sendData,false).then(res => {
            IM.sendCustomMessage({
              ...IM.options.ctx._imSendDefaultData,
              msgType: 72,
              content: res.allWatchNum,
              detailWatchNum: res.watchNum
            });
            clearTimeout(this._timer);
            this._timer = null;
            this._viewCount = 0;
            this.triggerEvent("changeCount",res.allWatchNum)
          })
        }, 1000)
      }
    }
  }
})
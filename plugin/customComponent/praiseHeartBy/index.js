import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    
  },
  /**
   * 组件的初始数据
   */
  data: {
    queueList:[],
    queueListMove:[],
    time: [],
    tipInfo: [],
    timeIndex: 0,
    numberToal:0,
    timepush:300,
    praiseHeartByCon:null,
    pushTime:null,
    mapNumber: {
      0: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco1.png",
      1: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco2.png",
      2: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco3.png",
      3: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco4.png",
      4: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco5.png",
      5: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco6.png",
      6: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco7.png",
      7: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco8.png",
      8: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco9.png",
      9: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco10.png",
      10: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco11.png",
      11: "https://ck-bkt-knowledge-payment.oss-cn-hangzhou.aliyuncs.com/admin/material/27_material_admin/image/images/dianzan/praiseIco12.png",
    },
  },

  /**
   * 组件的方法列表
   */
  detached: function(){
    clearInterval(this.data.pushTime)
    clearTimeout(this.data.praiseHeartByCon)
    this.setData({
      pushTime:null,
      praiseHeartByCon:null,
    })
  },
  methods: {
    // 添加提醒消息到队列
    public_addPraiseheartBy(Num) {
      if (this.data.praiseHeartByCon) {
        this.setData({
          numberToal:this.data.numberToal + Num
        })
      } else {
        this.data.numberToal = this.data.numberToal + Num
        this.data.praiseHeartByCon = setTimeout(() => {
          this.setData({
            queueListMove:[]//1秒内的点赞次数统计
          })
          for (let i = 0;i < 20 && i < this.data.numberToal;i++) {
            let msg = {}
            msg.time = Date.now() + i,
            msg.src = this.data.mapNumber[Math.ceil(Math.random() * 11)]
            if (this.data.numberToal <= 5) {
              msg.laytime = 3
            } else if (5 < this.data.numberToal && this.data.numberToal <= 10) {
              msg.laytime = 2
            } else if (10 < this.data.numberToal && this.data.numberToal <= 20) {
              msg.laytime = 1
            }
            this.data.queueListMove.push(msg);
            this.setData({
              queueListMove:this.data.queueListMove
            })
          }
          this.data.queueList.push(this.data.queueListMove)
          this.setData({
            queueList:this.data.queueList
          })
          // console.log(this.tipInfo)
          if (this.data.tipInfo.length == 0) {
            if (this.data.queueList[0][0].laytime == 3) {
              this.data.timepush = 800
            } else if (this.data.queueList[0][0].laytime == 2) {
              this.data.timepush = 600
            } else {
              this.data.timepush = 300
            }
            this.pushTimecon()
            // this.tipInfo = this.queueList[0];
            // this.queueList.shift();
          }
          this.data.numberToal = 0
          this.setData({
            numberToal:this.data.numberToal
          })
          clearTimeout(this.data.praiseHeartByCon)
          this.setData({
            praiseHeartByCon:null
          })
        }, 1000);
      }
    },
    //定时扔进动画数组
    pushTimecon() {
      let timeNewcont = 0
      // 例：this.queueList=[[{1},{2},{5},{1},{2},{6}],[{1},{2},{1},{1},{2},{1}]]
      this.data.pushTime = setInterval(() => {
        if (this.data.tipInfo && this.data.tipInfo[this.data.tipInfo.length - 1] != this.data.queueList[0][0].laytime && this.data.queueList[0][0].laytime == 3) {
          clearInterval(this.data.pushTime)
          // this.data.timepush = 800
          this.setData({
            pushTime:null,
            timepush:800
          })
          this.pushTimecon()
        } else if (this.data.tipInfo && this.data.tipInfo[this.data.tipInfo.length - 1] != this.data.queueList[0][0].laytime && this.data.queueList[0][0].laytime == 2) {
          clearInterval(this.data.pushTime)
          // this.data.timepush = 600
          this.setData({
            pushTime:null,
            timepush:600
          })
          this.pushTimecon()
        } else if (this.data.tipInfo && this.data.tipInfo[this.data.tipInfo.length - 1] != this.data.queueList[0][0].laytime && this.data.queueList[0][0].laytime == 1) {
          clearInterval(this.data.pushTime)
          this.setData({
            pushTime:null,
            timepush:300
          })
          // this.data.timepush = 300
          this.pushTimecon()
        }
        // console.log(timeNewcont)
        if (timeNewcont == 30) {
          clearInterval(this.data.pushTime)
          this.setData({
            pushTime:null,
          })
          this.pushTimecon()
        }
        this.data.tipInfo.push(this.data.queueList[0][0])
        this.setData({
          tipInfo:this.data.tipInfo
        })
        timeNewcont++
        this.data.queueList[0].shift()
        this.setData({
          queueList:this.data.queueList
        })
        if (this.data.queueList[0] && this.data.queueList[0].length == 0) {
          this.data.queueList.shift()
          this.setData({
            queueList:this.data.queueList
          })
        }
        if (this.data.queueList && this.data.queueList.length == 0) {
          clearInterval(this.data.pushTime)
          // this.data.pushTime == null
          this.setData({
            pushTime:null
          })
        }
      }, this.data.timepush)
    },
    afterQueueMessageLeaveBy() {
        this.data.tipInfo.shift();
        this.setData({
          tipInfo:this.data.tipInfo
        })
    }
  }
})  
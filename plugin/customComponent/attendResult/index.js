import utils from '../../utils/util';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    liveId:{
      type:Number,
      value:0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    info:{},
    acceptData:{},
    isShowCj:false, //是否显示抽奖弹框
    isShowWelfare:false, //是否显示福利中将弹框
  },

  /**
   * 组件的方法列表
   */
  methods: {
    show(data){
      this.data.acceptData =data;
      this.setData({
        acceptData:this.data.acceptData
      });
      if (this.data.acceptData.activityType == 64) {
        this.setData({
          isShowCj:true
        })
      } else if(this.data.acceptData.activityType == 128){
        this.setData({
          isShowWelfare:true
        })
      }
    },
    hideCj(){
      this.setData({
        isShowCj:false
      })
    },
    hideWelfare(){
      this.setData({
        isShowWelfare:false
      })
    },
    hideAll(){
      this.setData({
        isShowCj:false,
        isShowWelfare:false
      })
    },
    openCjPrize(){
      let acceptData = this.data.acceptData;
      utils.get(`live/lottery/drawDetail/${this.data.liveId}/${acceptData.id}`).then(res=>{
        res.lotteryId = acceptData.id;
        this.setData({
          info:res
        })
      })
    },
    openWelfarePrize(){

    },
    openRoster(){
      this.triggerEvent("openRoster",{id:this.data.acceptData.id})
    },
    //领取抽奖奖品
    getCjPrize(){
      let info = this.data.info;
      this.triggerEvent("getActivityPrize",{
        mgId:info.prodId,
        quantity:1,
        query:{
          fromType: this.data.acceptData.activityType,
          rId:info.rId,
          prizeId:info.prizeId
        }
      })
    },
    getWelfarePrize(){
      let acceptData = this.data.acceptData;
      this.triggerEvent("getActivityPrize",{
        mgId:acceptData.prodId,
        quantity:1,
        query:{
          fromType: this.data.acceptData.activityType,
          rId:acceptData.rId,
          prizeId:acceptData.prizeId
        }
      })
    },
  }
})

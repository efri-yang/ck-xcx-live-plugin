// businessComponent/switchTab/index.ts
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabIndex:{
      type:Number,
      value:1
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    tabList: [{
      title: "首页",
      val: 1
    }, {
      title: "我的",
      val: 2
    }]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    switchTab(e) {
      let val = e.currentTarget.dataset.val;
      let url = '';
      if (this.data.tabIndex == val) return;
      if (val == 1) {
        url = '/pages/index/index';
      } else if (val == 2) {
        url = '/pages/mine/index';
      }
      let currentPages = getCurrentPages();
      let delta='';
      for(let i=currentPages.length-1;i>=0;i--){
        if("/"+currentPages[i].route == url){
          console.log("i",i)
          delta = currentPages.length-i-1;
          break;
        }
      }
      if(delta){
        wx.navigateBack({
          delta:delta
        }) 
      }else{
        wx.navigateTo({
          url: url
        })
      }
    }
  }
})
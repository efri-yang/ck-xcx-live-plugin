
let plugin = requirePlugin("live-room");
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this._getAcceptDataFromOpenerPage().then(res=>{
      console.log("options",res)
      wx.requestPayment({
        timeStamp:res.timeStamp,
        nonceStr:res.nonceStr,
        package:res.package,
        signType:res.signType,
        paySign:res.paySign,
        success(res){
          console.log("res",res);
          plugin.setStorage("fromPage",1).then(()=>{
            wx.navigateBack();
          });
        },
        fail(res){
          console.log("res",res);
          wx.navigateBack();
        }
      })
    })
   
  },  

   //接收跳转页面传递的数据
   _getAcceptDataFromOpenerPage: function () {
    return new Promise((resolve, reject) => {
      const eventChannel = this.getOpenerEventChannel();
      if (Object.keys(eventChannel).length) {
        eventChannel.on('acceptDataFromOpenerPage', function (data) {
          resolve(data);
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
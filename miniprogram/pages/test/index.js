import utils from '../../utils/util';
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowTestPopup: false,
    isShowFormPopup:true,
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      isShowFormPopup:true
    })
    if(options.token){
      utils.setStorage("token",options.token);
    }
  },

  

  sexPickerChange(e){
    this.setData({
      sexIndex:e.detail.value
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
let app = getApp();
import utils from '../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    systemInfo:{},
    isLogin:false,
    dataInfo:{},
    isShowNicknameEdit:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
  
  },
  /**
   * 生命周期函数--监听页面显示
  */
  onShow(){
    let systemInfo= utils.getStorage("systemInfo");
    let isLogin=wx.getStorageSync('token');
    this._userSecret=wx.getStorageSync('userSecret')
    this.setData({
      systemInfo:systemInfo,
      isLogin:isLogin
    });
    if(!isLogin || !this._userSecret){
      utils.asyncLogin(true).then(()=>{
        this._userSecret=wx.getStorageSync('userSecret'); 
      })
    }else{
      utils.get('liveApplet/getUserInfo').then((res)=>{
        utils.setStorage("userInfo",res);
        res.mobile= res.mobile.replace(/(\d{3})\d{4}(\d{4})/, "$1****$2");
        this.setData({
          dataInfo:res
        })
      })
    }
    utils.get('imageSign').then(res => {
      this._aLiYunISP = res;
    });
  },
  openNicknameEdit(){
    this.setData({
      isShowNicknameEdit:true
    })
  },
  bindchooseavatar(e){
    console.log(e);
    let url = e.detail.avatarUrl;
    console.log("bindchooseavatar",url)
    let nameCreate = `xcx_avatar_${this._userSecret.openId}_${utils.uuid()}.${url.split(".")[1]}`;
    console.log(nameCreate)
    utils.upload({
      filePath: url,
      ispInfo: this._aLiYunISP,
      nameCreate: nameCreate
    }).then(res => {
      utils.post("liveApplet/updateUser",{
       avatarUrl:res
      }).then(()=>{
        this.setData({
          'dataInfo.avatarUrl':url
        })
      })
    })
  },
  nicknameBlur(e){
    console.log("e",e)
    let val = e.detail.value;
    this.setData({
      isShowNicknameEdit:false
    })
    if(val.replace(/^\s+|\s+$/g, '')){
      utils.post("liveApplet/updateUser",{
        nickName:val
      }).then(()=>{
        this.setData({
          'dataInfo.nickName':val
        })
      })
    }else{
      wx.showToast({
        title: '请输入当前的微信昵称',
        icon:"none",
        duration:2000
      })
    }
  },

  jumpToAuth(){
    utils.jumpToAuth('/pages/mine/index')
  },
  jumpToBindPhone(){
    if(!this.data.dataInfo.mobile){
      utils.jumpToAuth('/pages/mine/index')
    }
  },
  
  logout(){
    utils.removeStorage("token");
    utils.removeStorage("shopInfo");
    this.onShow();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },



  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
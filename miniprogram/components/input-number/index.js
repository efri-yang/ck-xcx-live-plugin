// components/input-number/index.ts
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    max:{
      type: Number,
      optionalTypes: [String],
      value: 9999
    },
    min:{
      type: Number,
      optionalTypes: [String],
      value: 1
    },
    val:{
      type: Number,
      optionalTypes: [String],
      value: 1
    },
    themeType:{
      type:String,
      optionalTypes: [Number],
      value:'default'
    }
  },
  lifetimes:{
    attached: function () {
      this.setData({
        numVal:this.data.val
      })
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    numVal:1
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindinput(e){
      let value= Number(e.detail.value);
      this.setData({
        numVal:value > 0 ? value : 1
      },()=>{
        this.triggerEvent("change",this.data.numVal);
      })
    },
    bindblur(e){
      let value= Number(e.detail.value);
      this.setData({
        numVal:value > 0 ? value : 1
      },()=>{
        this.triggerEvent("change",this.data.numVal);
      })
    },
    reduce(){
      let val= this.data.numVal-1;
      if(val < 1) val =1;
      this.setData({
        numVal:val
      },()=>{
        this.triggerEvent("change",val);
      })
    },
    add(){
      let val= this.data.numVal+1;
      this.setData({
        numVal:val
      },()=>{
        this.triggerEvent("change",val);
      })
    }
  }
})

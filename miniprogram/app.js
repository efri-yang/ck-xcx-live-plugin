App({
  onLaunch: function (options) {
    console.log("onLaunch-options:",options);
    wx.getSystemInfo({
      success: (res) => {
        let systemInfo = res;
        let navBarHeight = 0;
        let menuButtonInfo = wx.getMenuButtonBoundingClientRect();
        // if (/iphone\s{0,}x/i.test(systemInfo.model)) {
        //   navBarHeight = 88
        // } else if (systemInfo.system.indexOf('Android') !== -1) {
        //   navBarHeight = 68
        // } else {
        //   navBarHeight = 64
        // }
        navBarHeight =  systemInfo.statusBarHeight + menuButtonInfo.height + (menuButtonInfo.top - systemInfo.statusBarHeight) * 2;
      
        systemInfo.menuButtonInfo = menuButtonInfo;
        systemInfo.navBarHeight = navBarHeight;
        systemInfo.menuRight = systemInfo.screenWidth - menuButtonInfo.right;
        systemInfo.menuBottom = menuButtonInfo.top - systemInfo.statusBarHeight;
        systemInfo.menuHeight = menuButtonInfo.height;
        systemInfo.menuWidth = menuButtonInfo.width;
        systemInfo.menuTop = menuButtonInfo.top;
        wx.setStorageSync("systemInfo",systemInfo);
      }
    })
  },
  globalData:{
    isCK:true
  },
})